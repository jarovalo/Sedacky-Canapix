﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VoxelBusters.NativePlugins;

public class ScreenshotAR : MonoBehaviour
{
#if !UNITY_WEBGL && !UNITY_STANDALONE_WIN
    public GameObject[] deactivateOnScreenshot;
    public GameObject[] activateOnScreenshot;

    public void CaptureScreenshot()
    {
        foreach (GameObject go in deactivateOnScreenshot)
        {
            go.SetActive(false);
        }
        foreach (GameObject go in activateOnScreenshot)
        {
            go.SetActive(true);
        }

        NPBinding.MediaLibrary.SaveScreenshotToGallery(SaveImageToGalleryFinished);

        foreach (GameObject go in deactivateOnScreenshot)
        {
            go.SetActive(true);
        }
        foreach (GameObject go in activateOnScreenshot)
        {
            go.SetActive(false);
        }
    }

    private void SaveImageToGalleryFinished(bool _saved)
    {
        Debug.Log("Saved image to gallery successfully ? " + _saved);
    }

#endif
}
