﻿using System;
using System.IO;
using UnityEditor;

public class CreateAssetBundles
{
    [MenuItem("Yahart/Build AssetBundles Android")]
    static void BuildAllAssetBundlesAndroid()
    {
        string assetBundleDirectory = "Assets/StreamingAssets/AssetBundles";
        if (!Directory.Exists(assetBundleDirectory))
        {
            Directory.CreateDirectory(assetBundleDirectory);
        }

        BuildByTarget(BuildTarget.Android, assetBundleDirectory);
    }

    [MenuItem("Yahart/Build AssetBundles iOS")]
    static void BuildAllAssetBundlesIOS()
    {
        string assetBundleDirectory = "Assets/StreamingAssets/AssetBundles";
        if (!Directory.Exists(assetBundleDirectory))
        {
            Directory.CreateDirectory(assetBundleDirectory);
        }

        BuildByTarget(BuildTarget.iOS, assetBundleDirectory);
    }

    [MenuItem("Yahart/Build AssetBundles Windows")]
    static void BuildAllAssetBundlesWindows()
    {
        string assetBundleDirectory = "Assets/StreamingAssets/AssetBundles";
        if (!Directory.Exists(assetBundleDirectory))
        {
            Directory.CreateDirectory(assetBundleDirectory);
        }

        BuildByTarget(BuildTarget.StandaloneWindows, assetBundleDirectory);
    }

    [MenuItem("Yahart/Build AssetBundles WebGL")]
    static void BuildAllAssetBundlesWebGL()
    {
        string assetBundleDirectory = "Assets/StreamingAssets/AssetBundles";
        if (!Directory.Exists(assetBundleDirectory))
        {
            Directory.CreateDirectory(assetBundleDirectory);
        }

        BuildByTarget(BuildTarget.WebGL, assetBundleDirectory);
    }

    static void BuildByTarget(BuildTarget target,string dirName)
    {
        DateTime dateStart = DateTime.Now;

        if (!Directory.Exists(dirName))
        {
            Directory.CreateDirectory(dirName);
        }
        BuildPipeline.BuildAssetBundles(dirName, BuildAssetBundleOptions.None, target);
    }


}