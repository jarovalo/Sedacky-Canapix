﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollectionSelector : MonoBehaviour
{
    public AppConfigGlobalScriptableObject appConfigGlobal;

    public AppConfigScriptableObject[] appConfig;

    public string sceneMenu;

    public void SelectConfig(int index)
    {
        appConfigGlobal.appConfig = appConfig[index];
        SceneManager.LoadScene(sceneMenu);
    }
}
