﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OptionalFunctionEvent : UnityEvent<bool>
{
}

public class EventOnOptionalFunction : MonoBehaviour
{
    public PartOptionalFunctionScriptableObject[] optionalFunctions;
    public OptionalFunctionEvent eventOnOptionalFunction;

    // Start is called before the first frame update
    void Start()
    {
        Part part = GetComponentInParent<Part>();
        bool activate = optionalFunctions.Contains(part.selectedOptionalFunction);
        eventOnOptionalFunction.Invoke(activate);
    }
}
