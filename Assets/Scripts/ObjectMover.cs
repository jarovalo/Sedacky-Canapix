﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMover : MonoBehaviour {

    public Transform moveObject;

    public float speed = 0.05f;

    Quaternion rot;

    bool wasMoreFingers=false;

    void LateUpdate()
    {
        if (moveObject == null)
            return;

        if (Input.touchCount <= 1)
            wasMoreFingers = false;

        if (Input.touchCount >1 )
            wasMoreFingers = true;

        if (wasMoreFingers)
            return;

        if (Input.touchCount != 1)
            return;

        Touch touch = Input.GetTouch(0);

        
        if (touch.phase == TouchPhase.Began)
        {
            rot = Camera.main.transform.rotation;
            rot.eulerAngles = new Vector3(0f, rot.eulerAngles.y, 0f);
        }
          
        if (touch.phase==TouchPhase.Moved)
        {
            Vector3 pos = moveObject.position;
            Vector3 inputDir = new Vector3(touch.deltaPosition.x, 0f, touch.deltaPosition.y ) * Time.deltaTime * speed;
            pos += rot * inputDir;

            moveObject.position = pos;
        }
    }
}
