﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScaleModeAdjuster : MonoBehaviour
{
    private void Awake()
    {
        CanvasScaler canvasScaler=GetComponent<CanvasScaler>();
        if (Application.platform==RuntimePlatform.WebGLPlayer)
        {
            canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ConstantPhysicalSize;
        }
        else
        {
            canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
        }
    }
}
