﻿using Newtonsoft.Json.Linq;
using SubjectNerd.Utilities;
using UnityEngine;

[CreateAssetMenu(fileName = "Model", menuName = "ScriptableObjects/ModelScriptableObject", order = 1)]
public class ModelScriptableObject : ScriptableObject
{
    public string modelName;
    public string modelDescription;
    public float modelPrice;
    [Reorderable]
    public string[] parts;
    public Sprite image;
    public string imageUrl;
    public Sprite imageStock;
    public MaterialScriptableObject[] materials;
    public int materialDefaultIndex = 0;
    public MaterialScriptableObject[] materialsLegs;
    public MaterialScriptableObject[] materialsLegs2;
    public int materialCount = 1;
    public int legsCount = 1;
    public int sewingCount = 1;
    public uint assetBundleVersion = 0;
    public JObject configuredObject;
    public ModelScriptableObject[] variants;
    public Texture[] textureNormalSewing;
    public Sprite[] spriteLegs;

    public string AssetBundleName
    {
        get
        {
            return modelName.ToLower() + ".unity3d";
        }
    }
}
