﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Collections;
using Unity.EditorCoroutines.Editor;

public class GenerateThumbnails : EditorWindow
{
    Material materialMain;
    Material materialLegs;

    bool generateLegsThumbnail = false;

    // Add menu named "My Window" to the Window menu
    [MenuItem("Yahart/Generate Thumbnails")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        GenerateThumbnails window = (GenerateThumbnails)EditorWindow.GetWindow(typeof(GenerateThumbnails));
        window.Show();
    }

    void OnGUI()
    {
        generateLegsThumbnail = (bool)EditorGUILayout.Toggle("Generate legs thumbnail", generateLegsThumbnail);
        materialMain = (Material) EditorGUILayout.ObjectField("Material Main",materialMain, typeof(Material));
        materialLegs = (Material)EditorGUILayout.ObjectField("Material Legs",materialLegs, typeof(Material));

        Part[] parts = Selection.GetFiltered<Part>(SelectionMode.Assets);

        EditorGUILayout.LabelField(parts.Length + " parts selected.");

        if (parts.Length>0)
        {
            if (GUILayout.Button("Generate"))
            {
                EditorCoroutineUtility.StartCoroutine(Generate(parts), this);
            }
        }
        
    }

    IEnumerator Generate(Part[] parts)
    {
        foreach (Part part in parts)
        {
            int loopTimes = 1;
            if (generateLegsThumbnail)
            {
                loopTimes = 2;
            }

            for (int i=0;i<loopTimes;i++)
            {
                Debug.Log("Generating part " + part.name);

                Part newPart = Instantiate(part);

                GameObject remove = newPart.gameObject;

                PartMeshType[] partMeshTypes = newPart.GetComponentsInChildren<PartMeshType>(true);
                foreach (PartMeshType partMeshType in partMeshTypes)
                {
                    if (partMeshType.partMeshType == "main")
                    {
                        partMeshType.SetMaterial(materialMain, Color.white);
                    }
                    else
                    {
                        partMeshType.SetMaterial(materialLegs, Color.white);
                    }
                }

                int resolution = 1024;

                if (generateLegsThumbnail)
                {
                    resolution *= 4;

                    string findType = i == 0 ? "legs" : "legs2";
                    PartMeshType legs = partMeshTypes.Where(pmt => pmt.partMeshType == findType).FirstOrDefault();
                    if (legs != null)
                    {
                        legs.transform.SetParent(null);
                        legs.gameObject.SetActive(true);
                        remove = legs.gameObject;
                        DestroyImmediate(newPart.gameObject);

                        MeshRenderer meshRenderer = legs.GetComponent<MeshRenderer>();
                        legs.transform.position = -meshRenderer.bounds.center;
                    }
                    else
                    {
                        Debug.LogError("Part does not contain " + findType);
                    }
                }

                yield return null;

                RenderTexture rt = new RenderTexture(resolution, resolution, 24, RenderTextureFormat.ARGB32);
                Camera.main.targetTexture = rt;

                RenderTexture.active = rt;

                Camera.main.Render();

                Texture2D screenshot = new Texture2D(rt.width, rt.height, TextureFormat.ARGB32, false);
                screenshot.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
                screenshot.Apply();

                screenshot = Screenshot.RemoveBorders(screenshot, 256);

                string prefabPath = AssetDatabase.GetAssetPath(part);
                string path = System.IO.Directory.GetParent(System.IO.Directory.GetParent(prefabPath).FullName).FullName + "\\GUI\\" + System.IO.Path.GetFileNameWithoutExtension(prefabPath) + ".png";
                if (generateLegsThumbnail)
                {
                    path = System.IO.Directory.GetParent(System.IO.Directory.GetParent(prefabPath).FullName).FullName + "\\GUI\\legs" + (i==0 ? "" : "2") + ".png";
                }
                path = path.Substring(path.IndexOf("Assets\\"));
                path = AssetDatabase.GenerateUniqueAssetPath(path);

                System.IO.File.WriteAllBytes(path, screenshot.EncodeToPNG());

                AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);

                TextureImporter ti = AssetImporter.GetAtPath(path) as TextureImporter;
                ti.textureType = TextureImporterType.Sprite;
                ti.isReadable = true;
                ti.mipmapEnabled = false;

                EditorUtility.SetDirty(ti);
                ti.SaveAndReimport();

                if (generateLegsThumbnail==false)
                {
                    var spriteObject = AssetDatabase.LoadAssetAtPath<Sprite>(path);
                    part.image = spriteObject;
                }

                Debug.Log("Thumbnail saved to " + path);

                DestroyImmediate(remove, true);
            }
        }
    }

    
    


}