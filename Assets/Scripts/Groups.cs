﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Groups : MonoBehaviour
{
    public GameObject activateGroupAtStart;

    GameObject lastActiveGroup = null;

    private void Awake()
    {
        ActivateGroup(activateGroupAtStart);
    }

    public void ActivateGroup(GameObject group)
    {
        if (group == lastActiveGroup)
            return;

        foreach (Transform t in transform)
            t.gameObject.SetActive(t.gameObject == group);

        lastActiveGroup = group;
    }

}
