﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackRestActivator : MonoBehaviour
{
    public PartPoint partPointL;
    public PartPoint partPointR;

    public GameObject activateL;
    public GameObject activateR;
    public GameObject activateBoth;
    public GameObject activateNone;
    public GameObject activateNoneL;
    public GameObject activateNoneR;

    Part part;


    // Start is called before the first frame update
    void Start()
    {
        part = GetComponent<Part>();
        part.OnPartPointConnect.AddListener(Calculate);

        Calculate();
    }

    public void Calculate()
    {
        activateL.SetActive(false);
        activateR.SetActive(false);
        if (activateBoth != null)
            activateBoth.SetActive(false);
        if (activateNoneL != null)
            activateNoneL.SetActive(false);
        if (activateNoneR != null)
            activateNoneR.SetActive(false);

        bool none = true;

        if (activateBoth !=null && (( partPointL.ConnectedToConnection("op") && partPointR.ConnectedToConnection("ol")) || (partPointL.ConnectedToPartCategory("armr") && partPointR.ConnectedToPartCategory("arml"))))
        {
            activateBoth.SetActive(true);
            none = false;
        }
        else
        {
            if (activateR != null && (partPointL.ConnectedToConnection("op") || partPointL.ConnectedToPartCategory("armr")))
            {
                activateR.SetActive(true);
                none = false;
            }
            else
            {
                if (activateNoneR!=null)
                {
                    activateNoneR.SetActive(true);
                }
            }
            
            if (activateL != null && (partPointR.ConnectedToConnection("ol") || partPointR.ConnectedToPartCategory("arml")))
            {
                activateL.SetActive(true);
                none = false;
            }
            else
            {
                if (activateNoneL != null)
                {
                    activateNoneL.SetActive(true);
                }
            }

        }

        if (activateNone!=null)
            activateNone.SetActive(none);
    }
}
