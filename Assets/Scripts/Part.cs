﻿using System;
using System.CodeDom;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class Part : MonoBehaviour
{
    [Serializable]
    public class PartNameCombination
    {
        public Part[] connectedToParts;
        public string partName;
    }

    public string partName;
    public PartNameCombination[] partNameCombinations;

    public string partDescription;
    public string partCategory = "universal";

    public bool includeInDimensions = true;
    public Vector3 dimensions;
    public Sprite image;
    public int type = 0;
    
    
    public PartPoint[] partPoints;
    public bool canBeStandalone = true;

    public string[] sewingNames;

    public UnityEvent OnPartPointConnect;

    public PartOptionalFunctionScriptableObject[] possibleOptionalFunctions;
    
    [HideInInspector]
    public PartOptionalFunctionScriptableObject selectedOptionalFunction;

    ModelScriptableObject model;

    public void Init(ModelScriptableObject model, PartOptionalFunctionScriptableObject optionalFunction)
    {
        this.model = model;

        this.selectedOptionalFunction = optionalFunction;

        partPoints = GetComponentsInChildren<PartPoint>();
        Color color = Color.white;
        if (model.materials[0].colors != null)
        {
            color = model.materials[0].colors.colors[0].color;
        }
        SetMaterial(model.materials[0].material, color,"main");
    }

    public void Disconnect()
    {
        foreach(PartPoint partPoint in partPoints)
        {
            partPoint.Disconnect();
        }
    }

    public bool Connected
    {
        get
        {
            bool connected = false;
            foreach (PartPoint partPoint in partPoints)
            {
                if (partPoint.Connected)
                {
                    connected = true;
                    break;
                }
            }
            return connected;
        }
    }

    public void SetMaterial(Material material,Color color,string meshType)
    {
        Debug.Log("SetMaterial " + gameObject.name + " - " + meshType+" - " +material.name+" - " + color);

        PartMeshType[] pmts = GetComponentsInChildren<PartMeshType>(true);
        foreach (PartMeshType pmt in pmts)
        {
            if (pmt.partMeshType == meshType)
            {
                pmt.SetMaterial(material, color);

                for (int i=0;i < sewingNames.Length;i++) 
                {
                    if (pmt.name.EndsWith(sewingNames[i]))
                    {
                        if (pmt.sewing)
                        {
                            pmt.SetSecondaryNormal(model.textureNormalSewing[i]);
                        }
                    }
                }
            }
                
        }
    }

    public Bounds PartBoundsMesh()
    {
        MeshCollider[] mcs = GetComponentsInChildren<MeshCollider>();
        Bounds bounds = new Bounds();
        foreach (MeshCollider mc in mcs)
        {
            Mesh m = mc.sharedMesh;
            mc.sharedMesh = null;
            mc.sharedMesh = m;

            bounds.Encapsulate(mc.bounds);
        }
            

        return bounds;
    }

    public Bounds PartBoundsDimensions()
    {
        Bounds b = new Bounds(Vector3.zero, Vector3.zero);
        Renderer[] renderers = GetComponentsInChildren<Renderer>();
        foreach(Renderer r in renderers)
        {
            if (b.size == Vector3.zero) 
            {
                b = r.bounds;
            }
            else
            {
                b.Encapsulate(r.bounds);
            }
        }
        return b;
    }

    public bool CanConnectTo(PartPoint partPointToConnect,PartPointConnection[] connections)
    {
        PartPointConnection connection = connections.Where(conn => conn.canConnectTo.Contains(partPointToConnect.connection)).First();
        bool canConnect = partPoints.Where(partP => partP.connection == connection.connection && partP.type == partPointToConnect.type).Count() > 0;
        if (canConnect && (partCategory !="universal" && partPointToConnect.part.partCategory!="universal") || partPointToConnect.allowedCategories.Count()>0)
        {
            canConnect = partPointToConnect.allowedCategories.Contains(partCategory);
        }
        if (canConnect && partPointToConnect.disallowedCategories.Count() > 0)
        {
            canConnect = partPointToConnect.disallowedCategories.Contains(partCategory)==false;
        }
        return canConnect;
    }

    public void SetSewing(int index)
    {
        PartMeshType[] partMeshTypes = transform.GetComponentsInChildren<PartMeshType>(true);
        foreach(PartMeshType partMeshType in partMeshTypes)
        {
            if (partMeshType.name.EndsWith(sewingNames[0]))
            {
                for (int i=0;i<sewingNames.Count()+1;i++)
                {
                    string findingName=partMeshType.name.Replace(sewingNames[0],"");
                    if (i>0)
                    {
                        findingName = partMeshType.name.Replace(sewingNames[0], sewingNames[i - 1]);
                    }
                    PartMeshType pmt = partMeshTypes.Where(p => p.name == findingName).FirstOrDefault();
                    if (pmt!=null)
                    {
                        pmt.gameObject.SetActive(i == index);
                    }
                }
            }
        }
    }

    public string GeneratedName
    {
        get
        {
            // change name if connected to all needed parts
            var partNameCombinationsSorted = partNameCombinations.OrderByDescending(p => p.connectedToParts.Length);
            foreach(PartNameCombination partNameCombination in partNameCombinationsSorted)
            {
                bool all = true;

                var connectedPartPoints = partPoints.Where(pp => pp.connectedTo != null);

                foreach(Part p in partNameCombination.connectedToParts)
                {
                    if (connectedPartPoints.Where(pp=>pp.connectedTo.part.partName==p.partName).Count()==0)
                    {
                        all = false;
                        break;
                    }
                }

                if (all)
                {
                    return partNameCombination.partName;
                }
            }

            // change name to "" if connected to other part that changed name
            foreach(PartPoint partPoint in partPoints)
            {
                if (partPoint.connectedTo!=null)
                {
                    foreach (PartNameCombination partNameCombination in partPoint.connectedTo.part.partNameCombinations)
                    {
                        bool all = true;

                        var connectedPartPoints = partPoint.connectedTo.part.partPoints.Where(pp => pp.connectedTo != null);

                        foreach (Part p in partNameCombination.connectedToParts)
                        {
                            if (connectedPartPoints.Where(pp => pp.connectedTo.part.partName == p.partName).Count() == 0)
                            {
                                all = false;
                                break;
                            }
                        }

                        if (all)
                        {
                            if (partNameCombination.connectedToParts.Where(ctp=>ctp.partName==partName).Count()>0)
                            {
                                return "";
                            }
                        }
                    }
                }
            }

            return partName;
        }


    }

}
