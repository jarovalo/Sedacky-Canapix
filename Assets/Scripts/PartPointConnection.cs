﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PartPointConnection
{
    public string connection;
    public string[] canConnectTo;
}

