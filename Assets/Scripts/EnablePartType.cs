﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnablePartType : MonoBehaviour
{
    public int partType = 2;
    public bool enable = false;

    private void OnEnable()
    {
        Configurator.instance.EnablePartType(partType, enable);
    }

    private void OnDisable()
    {
        Configurator.instance.EnablePartType(partType, enable == false);
    }
}
