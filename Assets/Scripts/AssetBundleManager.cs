﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class AssetBundleManager : MonoBehaviour
{
    public static AssetBundleManager instace;

    public AppConfigGlobalScriptableObject appConfigGlobal;

    UnityWebRequestAsyncOperation asyncOperation;

    public List<Part> parts;

    private void Awake()
    {
        AssetBundleManager.instace = this;
    }

    public void LoadModelAssetBundle(ModelScriptableObject model)
    {
        StartCoroutine(LoadModelAssetBundleCoroutine(model));
    }

    IEnumerator LoadModelAssetBundleCoroutine(ModelScriptableObject model)
    {
        yield return null;

        Configurator.model = model;

        if (Application.platform == RuntimePlatform.WebGLPlayer)
            StartCoroutine(LoadModelAssetBundleWeb(model));
        else
            LoadModelAssetBundleFromFile(model);

        LoadModelAssetBundleFromFile(model);
    }

    void LoadModelAssetBundleFromFile(ModelScriptableObject model)
    {
        AssetBundle assetBundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath,"AssetBundles",appConfigGlobal.appConfig.companyId, model.AssetBundleName));
        ProcessAssetBundle(assetBundle);
    }

    IEnumerator LoadModelAssetBundleWeb(ModelScriptableObject model)
    {
        //string uri = appConfigGlobal.appConfig.assetBundlesUrl + "/" + model.AssetBundleName;
        string uri = Path.Combine(Application.streamingAssetsPath, "AssetBundles", appConfigGlobal.appConfig.companyId, model.AssetBundleName);

        Debug.Log("Downloading " + uri);

        using (UnityWebRequest uwr = UnityWebRequestAssetBundle.GetAssetBundle(uri, model.assetBundleVersion, 0))
        {
            asyncOperation = uwr.SendWebRequest();
            yield return asyncOperation;

            if (uwr.isNetworkError || uwr.isHttpError)
            {
                Debug.Log(uwr.error);
            }

            // Get downloaded asset bundle
            AssetBundle assetBundle = DownloadHandlerAssetBundle.GetContent(uwr);
            ProcessAssetBundle(assetBundle);
        }
    }

    void ProcessAssetBundle(AssetBundle assetBundle)
    {
        parts = new List<Part>();
        foreach (string p in Configurator.model.parts)
        {
            GameObject go = assetBundle.LoadAsset<GameObject>(p + ".prefab");
            parts.Add(go.GetComponent<Part>());
        }
        assetBundle.Unload(false);
        UnityEngine.SceneManagement.SceneManager.LoadScene(appConfigGlobal.appConfig.configuratorScene);
    }

}
