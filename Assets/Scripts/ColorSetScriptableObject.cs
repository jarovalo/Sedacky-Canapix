﻿using SubjectNerd.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ColorSet", menuName = "ScriptableObjects/ColorSetScriptableObject", order = 4)]
public class ColorSetScriptableObject : ScriptableObject
{
    [Reorderable]
    public ColorSet[] colors;

    public bool orderByName = false;
}
