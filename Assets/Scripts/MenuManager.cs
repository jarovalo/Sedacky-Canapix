﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public AppConfigGlobalScriptableObject appConfigGlobal;

    public ModelGUI modelGUIPrefab;

    public Image[] imagesLogo;
    public Image imageLogoMenu;
    public Image imageBackground;

    public StockManager stockManager;

    public Text textVersion;

    public GameObject languageChanging;
    public GameObject[] languageFlags;

    public GameObject buttonSelection;

    private void Awake()
    {
        textVersion.text = Application.version;

        foreach (Image imageLogo in imagesLogo)
            imageLogo.sprite = appConfigGlobal.appConfig.spriteLogo;

        if (imageLogoMenu != null)
            imageLogoMenu.gameObject.SetActive(appConfigGlobal.appConfig.showMenuLogo);

        if (appConfigGlobal.appConfig.spriteMenuBackground != null)
        {
            if (Application.platform == RuntimePlatform.WebGLPlayer)
                imageBackground.sprite = appConfigGlobal.appConfig.spriteMenuBackgroundWeb;
            else
                imageBackground.sprite = appConfigGlobal.appConfig.spriteMenuBackground;
        }
        else
        {
            imageBackground.color = appConfigGlobal.appConfig.colorMenuBackground;
        }

        if (buttonSelection!=null)
            buttonSelection.SetActive(appConfigGlobal.appConfig.showSelection);

        ListModels();

        if (SaveManager.fileName!="")
        {
            stockManager.ShowStock();
        }

        SetFlagIcon();
        languageChanging.SetActive(appConfigGlobal.appConfig.allowLanguageChanging);
        if (appConfigGlobal.appConfig.forceLanguage!="")
        {
            LocalizationManager.language = appConfigGlobal.appConfig.forceLanguage;
        }
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
            Application.Quit();
    }

    void ListModels()
    {
        foreach(ModelScriptableObject model in appConfigGlobal.appConfig.modelList)
        {
            ModelGUI newModelGUI = Instantiate(modelGUIPrefab,modelGUIPrefab.transform.parent);
            newModelGUI.gameObject.SetActive(true);
            newModelGUI.Init(model);
        }
        modelGUIPrefab.gameObject.SetActive(false);
    }

    public void ChangeLanguage()
    {
        int index =System.Array.IndexOf( LocalizationManager.availableLanguages, LocalizationManager.language);
        index++;
        if (index >= LocalizationManager.availableLanguages.Length)
            index = 0;
        LocalizationManager.language = LocalizationManager.availableLanguages[index];
        SetFlagIcon();
    }

    void SetFlagIcon()
    {
        int index = System.Array.IndexOf(LocalizationManager.availableLanguages, LocalizationManager.language);

        for (int flag = 0; flag < languageFlags.Length; flag++)
        {
            languageFlags[flag].SetActive(flag == index);
        }
    }
}
