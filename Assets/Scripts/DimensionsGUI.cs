﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DimensionsGUI : MonoBehaviour
{
    public AppConfigGlobalScriptableObject appConfigGlobal;

    public RectTransform dimensionsLength;
    public RectTransform dimensionsDepth;
    public RectTransform dimensionsDepthLeft;

    public TextMeshProUGUI textDimensionLength;
    public TextMeshProUGUI textDimensionDepth;
    public TextMeshProUGUI textDimensionDepthLeft;

    public void SetDimensions(Vector3 center, float width,float depth,float depthLeft)
    {
        float maxDepth = Mathf.Max(depth, depthLeft);
        if (dimensionsLength != null)
        {
            textDimensionLength.text = DimensionToString(width);
            dimensionsLength.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
            Vector3 pos = new Vector3(center.x, dimensionsLength.transform.parent.position.y, center.z + (maxDepth / 2f) + dimensionsLength.sizeDelta.y / 2f);
            dimensionsLength.SetPositionAndRotation(pos, dimensionsLength.rotation);
            dimensionsLength.gameObject.SetActive(width > 0f);
        }
        if (dimensionsDepth != null)
        {
            textDimensionDepth.text = DimensionToString(depth);
            dimensionsDepth.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, depth);
            Vector3 pos = new Vector3(center.x + (width / 2f) + dimensionsDepth.sizeDelta.y / 2f, dimensionsDepth.transform.parent.position.y, center.z);
            dimensionsDepth.SetPositionAndRotation(pos, dimensionsDepth.rotation);
            dimensionsDepth.gameObject.SetActive(depth > 0f);
        }
        if (dimensionsDepthLeft != null)
        {
            textDimensionDepthLeft.text = DimensionToString(depthLeft);
            dimensionsDepthLeft.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, depthLeft);
            Vector3 pos = new Vector3(center.x - (width / 2f) - dimensionsDepthLeft.sizeDelta.y / 2f, dimensionsDepthLeft.transform.parent.position.y, center.z);
            dimensionsDepthLeft.SetPositionAndRotation(pos, dimensionsDepthLeft.rotation);
            dimensionsDepthLeft.gameObject.SetActive(depthLeft > 0f);
        }

    }

    string DimensionToString(float dimension)
    {
        dimension = Mathf.Round(dimension / appConfigGlobal.appConfig.dimensionsScale * 100f) / 100f;
        if (appConfigGlobal.appConfig.dimensionsScale<1f)
        {
            dimension = Mathf.Round(dimension);
        }
        return dimension.ToString() + " " + appConfigGlobal.appConfig.dimensionsUnits;
    }

    public void Hide()
    {
        dimensionsLength.gameObject.SetActive(false);
        dimensionsDepth.gameObject.SetActive(false);
        dimensionsDepthLeft.gameObject.SetActive(false);
    }
}
