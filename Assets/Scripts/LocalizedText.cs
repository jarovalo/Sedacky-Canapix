﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LocalizedText : MonoBehaviour {

    [SerializeField]
    LocalizationManager.Localization[] localizations;

    public void SetLocalizedTexts(Dictionary<string,string> texts)
    {
        localizations = new LocalizationManager.Localization[texts.Count];
        int index = -1;
        foreach(KeyValuePair<string,string> text in texts)
        {
            index++;
            localizations[index] = new LocalizationManager.Localization(text);
        }

        Translate();
    }

    public void SetLocalizedTexts(LocalizationManager.Localization[] localizations)
    {
        this.localizations = localizations;

        Translate();
    }

    private void OnEnable()
    {
        Translate();
    }

    public void Translate()
    {
        string localizedText= LocalizationManager.GetLocalizedText(LocalizationManager.LocalizationsToDictionary(localizations));
        Text text = GetComponent<Text>();
        if (text != null)
        {
            text.text = localizedText;
        }
        else
        {
            TextMeshProUGUI tmProUGUI = GetComponent<TextMeshProUGUI>();
            tmProUGUI.text = localizedText;
        }
    }
}
