﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTransform : MonoBehaviour
{
    public Transform follow;

    private void Update()
    {
        if (follow == null)
            return;

        Vector3 pos = Camera.main.WorldToScreenPoint(follow.position);
        transform.position = pos;
    }
}
