﻿using SubjectNerd.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AppConfig", menuName = "ScriptableObjects/AppConfigScriptableObject", order = 100)]
public class AppConfigScriptableObject : ScriptableObject
{
    [Reorderable]
    public ModelScriptableObject[] modelList;
    public List<Sprite> partPointIcons;
    public PartPointConnection[] partPointConnections;
    public string[] mailRecipients;
    public bool mailRecipientByUser = false;
    public string mailSubject= "Dopyt z aplikácie";
    public string mailBody="";
    public Sprite spriteLogo;
    public Sprite spriteMenuBackground;
    public Sprite spriteMenuBackgroundWeb;
    public Color colorMenuBackground;
    public string assetBundlesUrl;
    public string imageUploaderUrl;
    public string stockUrl;
    public string usersUrl;
    public string companyId;
    public bool allowLanguageChanging = true;
    public string forceLanguage = "";
    public string menuScene="menu";
    public string configuratorScene = "configurator";
    public float dimensionsScale = 1f;
    public string dimensionsUnits = "m";
    public bool showSelection = false;
    public bool showMenuLogo = true;
    public bool hideUncompatibleParts = false;
    public bool allowMultipleMaterialTypes = true;
    public bool showSeatComfort = false;
}
