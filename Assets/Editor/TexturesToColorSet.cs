﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TexturesToColorSet : EditorWindow
{
    Object colorSetObject;
    //Material originalMaterial;

    // Add menu named "My Window" to the Window menu
    [MenuItem("Yahart/Textures to color set")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        TexturesToColorSet window = (TexturesToColorSet)EditorWindow.GetWindow(typeof(TexturesToColorSet));
        window.Show();
    }

    void OnGUI()
    {
        colorSetObject = EditorGUILayout.ObjectField("Color set", colorSetObject, typeof(Object));
        //originalMaterial = (Material)EditorGUILayout.ObjectField("Original material", originalMaterial, typeof(Material));

        Texture2D[] textures = Selection.GetFiltered<Texture2D>(SelectionMode.Assets);

        EditorGUILayout.LabelField(textures.Length + " textures selected.");

        if (textures.Length > 0)
        {
            if (GUILayout.Button("Generate"))
            {
                List<ColorSet> colorSets=new List<ColorSet>();

                foreach (Texture2D texture in textures)
                {
                    ColorSet colorSet = new ColorSet();
                    colorSet.color = texture.GetPixel(texture.width / 2, texture.height / 2);
                    string name = texture.name;
                    if (name.Contains("_"))
                    {
                        string[] nameParts = name.Split('_');
                        name = nameParts[nameParts.Length - 1];
                    }
                    colorSet.colorName = name;
                    colorSets.Add(colorSet);
                    //Material newMaterial =(Material) PrefabUtility.InstantiatePrefab(originalMaterial);

                }

                ColorSetScriptableObject colorSetSO = (ColorSetScriptableObject)colorSetObject;
                colorSets.Sort((a, b) => a.colorName.CompareTo(b.colorName));
                colorSetSO.colors = colorSets.ToArray();
                EditorUtility.SetDirty(colorSetSO);
            }
        }
    }
}
