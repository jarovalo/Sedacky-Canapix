﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ButtonSelectable : MonoBehaviour
{
    public GameObject selected;
    public GameObject unselected;

    public bool unselectAfterOtherSelected = true;

    ButtonSelectable[] all;

    bool isSelected = false;

    private void Awake()
    {
        all = GameObject.FindObjectsOfType<ButtonSelectable>().Where(bs=>bs.unselectAfterOtherSelected==true).ToArray();

        Button b = GetComponent<Button>();
        b.onClick.AddListener(OnClick);

        Select(false);
    }

    void OnClick()
    {
        if (unselectAfterOtherSelected)
        {
            foreach (ButtonSelectable b in all)
            {
                b.Select(b == this);
            }
        }
        else
        {
            Select(isSelected == false);
        }
    }

    public void Select(bool select, bool immediate=true)
    {
        isSelected = select;

        if (gameObject.activeInHierarchy && immediate==false)
        {
            StartCoroutine(SelectCoroutine(select));
        }
        else
        {
            selected.SetActive(select);
            unselected.SetActive(select == false);
        }
    }

    IEnumerator SelectCoroutine(bool select)
    {
        yield return null;

        selected.SetActive(select);
        unselected.SetActive(select == false);
    }
}
