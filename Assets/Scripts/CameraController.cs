﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CameraController : MonoBehaviour
{
    public GameObject camera2D;
    public GameObject camera3D;

    public static bool IsCamera2D = true;

    private void Awake()
    {
        camera2D.SetActive(true);
        camera3D.SetActive(false);
    }

    public void SwitchTo2DView()
    {
        IsCamera2D = true;
        camera2D.SetActive(true);
        camera3D.SetActive(false);
    }

    public void SwitchTo3DView()
    {
        IsCamera2D = false;
        camera2D.SetActive(false);
        camera3D.SetActive(true);
    }


}
