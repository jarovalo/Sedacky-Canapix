﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GUIEvents : MonoBehaviour
{
    public UnityEvent OnPointerDownGUI;
    public UnityEvent OnPointerUpGUI;

    bool overGUI=false;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            overGUI = Utils.IsPointerOverUIObject();

            if (overGUI)
                OnPointerDownGUI.Invoke();
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (overGUI)
                OnPointerUpGUI.Invoke();
        }
    }
}
