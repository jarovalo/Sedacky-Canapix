﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class ToggleOnClick : MonoBehaviour
{
    public GameObject ActivateObject;
    public bool deactivateOnStart = false;
    public GameObject DeactivateObjectInsteadOfThis;

    private void OnMouseUp()
    {
        if (CameraController.IsCamera2D)
            return;

        if (this.enabled == false)
            return;

        ActivateObject.SetActive(true);
        if (DeactivateObjectInsteadOfThis == null)
            gameObject.SetActive(false);
        else
            DeactivateObjectInsteadOfThis.SetActive(false);
    }

    private void Start()
    {
        if (deactivateOnStart)
        {
            if (DeactivateObjectInsteadOfThis == null)
                gameObject.SetActive(false);
            else
                DeactivateObjectInsteadOfThis.SetActive(false);
        }
            
    }

    public void Toggle()
    {
        OnMouseUp();
    }
}
