﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AppConfigGlobal", menuName = "ScriptableObjects/AppConfigGlobalScriptableObject", order = 101)]
public class AppConfigGlobalScriptableObject : ScriptableObject
{
    public AppConfigScriptableObject appConfig;

    public bool adminApp=false;
}