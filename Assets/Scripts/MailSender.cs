﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using VoxelBusters.NativePlugins;

public class MailSender : MonoBehaviour
{
    public Configurator configurator;
    public Screenshot screenshot;
    public GameObject mailSenderGUI;
    public Button buttonSend;
    public Text textInfo;
    public bool PrivacyCheckRequired = true;
    public TMPro.TMP_InputField inputName;
    public TMPro.TMP_InputField inputPhone;
    public TMPro.TMP_InputField inputEmail;
    public TMPro.TMP_InputField inputPrice;
    public TMPro.TMP_InputField inputMaterial;
    public TMPro.TMP_InputField inputNote;
    public GameObject seatComfort;
    public TMP_Dropdown dropdownSeatComfort;

    private void Awake()
    {
        screenshot.ScreenshotsTaken += OnScreenshotsTaken;
    }

    private void OnDestroy()
    {
        screenshot.ScreenshotsTaken -= OnScreenshotsTaken;
    }

    // Start is called before the first frame update
    void Start()
    {
        mailSenderGUI.SetActive(false);
        if (PrivacyCheckRequired)
            buttonSend.interactable = false;
    }

    public void Show()
    {
        textInfo.text = "";
        mailSenderGUI.SetActive(true);
        if (inputMaterial!=null)
            inputMaterial.text = configurator.GetMaterialName();
        seatComfort.SetActive(configurator.appConfigGlobal.appConfig.showSeatComfort);
    }

    public void PrivacyChecked(bool on)
    {
        buttonSend.interactable = on;
    }

    public void ButtonCancel()
    {
        mailSenderGUI.SetActive(false);
    }

    public void ButtonSend()
    {
        textInfo.text = "";
        if (inputName.text.Trim().Length==0)
        {
            textInfo.text = "Údaj meno je povinný!";
            return;
        }
        if (inputPhone.text.Trim().Length == 0)
        {
            textInfo.text = "Údaj telefón je povinný!";
            return;
        }

        screenshot.TakeScreenshots();

        mailSenderGUI.SetActive(false);

        if (inputEmail!=null && configurator.appConfigGlobal.appConfig.usersUrl !="" ) 
            StartCoroutine( CheckUserEmail(inputEmail.text) );
    }

    bool IsScreenshotServiceAvailable
    {
        get
        {
#if UNITY_IOS
            return NPBinding.Sharing.IsMailServiceAvailable();
#else
            return false;
#endif
        }
    }

    private void OnFinishedSharing(eShareResult _result)
    {
        // Insert your code
    }

    void OnScreenshotsTaken(Texture2D[] screenshots)
    {
        if (IsScreenshotServiceAvailable)
        {
            StartCoroutine(SendMailCoroutine(screenshots));
        }
        else
        {
            StartCoroutine(SendMailtoCoroutine(screenshots[0]));
        }
    }

    IEnumerator SendMailCoroutine(Texture2D[] screenshots)
    {
        // Create new instance and populate fields
        MailShareComposer _composer = new MailShareComposer();

        string[] recipient = null;
        if (configurator.appConfigGlobal.appConfig.mailRecipientByUser)
        {
            recipient = new string[] { inputEmail.text };
        }
        else
        {
            recipient = configurator.appConfigGlobal.appConfig.mailRecipients;
        }

        _composer.ToRecipients = recipient;
        _composer.Subject = MailSubject;
        _composer.Body = MailBody;

        // Adding screenshot as attachment
        
        foreach (Texture2D t in screenshots)
            _composer.AttachImage(t);

        while (_composer.IsReadyToShowView == false)
            yield return null;

        // Show composer
#if UNITY_IOS
        NPBinding.Sharing.ShowView(_composer, OnFinishedSharing);
#endif
    }

    string MailSubject
    {
        get
        {
            return configurator.appConfigGlobal.appConfig.mailSubject;
        }
    }

    string MailBody
    {
        get
        {
            string body = configurator.appConfigGlobal.appConfig.mailBody.Replace("\\n",Environment.NewLine);
            body += "Meno: " + inputName.text + Environment.NewLine;
            body += "Telefón: " + inputPhone.text + Environment.NewLine;
            if (inputEmail!=null)
            {
                body += "E-mail: " + inputEmail.text + Environment.NewLine;
            }

            body += Environment.NewLine + "Zostava sedačky:" + Environment.NewLine;
            body += "Model: " + Configurator.model.modelName + Environment.NewLine;
            
            if (Configurator.model.configuredObject!=null)
            {
                body += Environment.NewLine+ "SKLAD" + Environment.NewLine;
                body += "Predajňa: " + Configurator.model.configuredObject["location"].ToString() + Environment.NewLine;
                body += "Cena: " + Configurator.model.configuredObject["price"].ToString() + Environment.NewLine;
                body += "Poťah: " + Configurator.model.configuredObject["description_material"].ToString() + Environment.NewLine;
            }
            else
            {
                if (inputMaterial==null)
                {
                    body += "Poťah: " + configurator.GetMaterialName() + Environment.NewLine;
                }
                else
                {
                    body += "Poťah: " + inputMaterial.text + Environment.NewLine;
                }
            }

            if (configurator.appConfigGlobal.appConfig.showSeatComfort)
            {
                body += "Komfort sedenia: " +dropdownSeatComfort.options[dropdownSeatComfort.value].text+Environment.NewLine;
            }

            body += Environment.NewLine;
            body += "Elementy: " + Environment.NewLine;
            foreach (Part part in configurator.parts)
            {
                string partName = part.GeneratedName;
                if (partName!="")
                {
                    string optionalFunction = "";
                    if (part.selectedOptionalFunction!=null)
                    {
                        optionalFunction = " , voliteľná funkcia: " + LocalizationManager.GetLocalizedText(part.selectedOptionalFunction.localizations);
                    }
                    body += "- " + partName + optionalFunction + Environment.NewLine;
                }
            }

            if (inputPrice!=null)
            {
                body += Environment.NewLine + "Cena: " + inputPrice.text + Environment.NewLine;
            }

            if (inputNote!=null)
            {
                body += Environment.NewLine + "Poznámka: " + inputNote.text + Environment.NewLine;
            }
            
            return body;
        }
    }

    IEnumerator SendMailtoCoroutine(Texture2D screenshot)
    {
        string screenshotUrl = "";
        string screenshotBase64 = Convert.ToBase64String(screenshot.EncodeToPNG());
        string url =configurator.appConfigGlobal.appConfig.imageUploaderUrl+ "/saveimage.php";
        Dictionary<string, string> formFields = new Dictionary<string, string>();
        formFields.Add("img", screenshotBase64);
        UnityWebRequest www = UnityWebRequest.Post(url, formFields);
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Debug.LogError("UploadImage Error: " + www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
            screenshotUrl = configurator.appConfigGlobal.appConfig.imageUploaderUrl +"/"+ www.downloadHandler.text;
        }

        string subject = MyEscapeURL(MailSubject);

        string body = MailBody;
        body += Environment.NewLine + Environment.NewLine + "Screenshot: " + screenshotUrl;
        body = MyEscapeURL(body);

        string recipient = "";
        if (configurator.appConfigGlobal.appConfig.mailRecipientByUser)
        {
            recipient = inputEmail.text;
        }
        else
        {
            recipient = String.Join(",", configurator.appConfigGlobal.appConfig.mailRecipients);
        }
        string mailtoUrl = "mailto:" + recipient + "?subject=" + subject + "&body=" + body;
        Debug.Log(mailtoUrl);
        Application.OpenURL(mailtoUrl);
    }

    string MyEscapeURL(string url)
    {
        return WWW.EscapeURL(url).Replace("+", "%20");
    }

    IEnumerator CheckUserEmail(string email)
    {
        UnityWebRequest www = UnityWebRequest.Get(configurator.appConfigGlobal.appConfig.usersUrl);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            // Show results as text
            //Debug.Log(www.downloadHandler.text);

            JObject joList = JObject.Parse(www.downloadHandler.text);

            if (joList["version"] != null)
            {
                int requiredVersion = joList["version"].Value<int>();
                if (int.Parse(Application.version.Replace(".", "")) < requiredVersion)
                {
                    Debug.LogError("Newer version required for user check...");
                    yield break;
                }
            }

            foreach (JToken user in joList["dealers"])
            {
                if (user.Value<string>()==email)
                {
                    Debug.Log("Dealer mail entered");
                    PlayerPrefs.SetInt("dealer", 1);
                    yield break;
                }
            }

            Debug.Log("No dealer mail entered");
        }
    }
}
