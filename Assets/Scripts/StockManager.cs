﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class StockManager : MonoBehaviour
{
    public AppConfigGlobalScriptableObject appConfig;
    public ModelGUI modelGUIPrefab;
    public GameObject loading;
    public GameObject newerVersionRequired;
    public GameObject scrollList;

    public Toggle toggleFilterCorner;
    public Toggle toggleFilterU;
    public Toggle toggleFilter321;

    float lastLoad = float.MinValue;

    public Groups groups;
    public GameObject groupStock;
    

    List<ModelScriptableObject> models;
    List<string> files;

    static bool toggleFilterCornerIsOn=true;
    static bool toggleFilterUIsOn=true;
    static bool toggleFilter321IsOn=true;

    bool listEnabled = true;

    private void Awake()
    {
        listEnabled = false;
        toggleFilterCorner.isOn = toggleFilterCornerIsOn;
        toggleFilterU.isOn = toggleFilterUIsOn;
        toggleFilter321.isOn = toggleFilter321IsOn;
        listEnabled = true;
        newerVersionRequired.SetActive(false);
    }

    public void ShowStock()
    {
        groups.ActivateGroup(groupStock);
        Reload();
    }

    void Reload()
    {
        bool reloadFromDisk = appConfig.adminApp;
        if (reloadFromDisk)
        {
            ReloadFromDisk();
        }
        else
        {
            ReloadFromServer();
        }
    }

    void ReloadFromDisk()
    {
        loading.SetActive(false);
        models = new List<ModelScriptableObject>();
        files = new List<string>();
        string fileList= SaveManager.DirPath() + Path.DirectorySeparatorChar + "list.json";
        if (File.Exists(fileList))
        {
            JObject joList = JObject.Parse(File.ReadAllText(fileList));
            foreach (JToken file in joList["files"])
            {
                string fileName = SaveManager.DirPath() + Path.DirectorySeparatorChar + file["file"].ToString();
                files.Add(fileName);
                string content = File.ReadAllText(fileName + ".json");
                byte[] contentImage = File.ReadAllBytes(Path.GetDirectoryName(fileName) + Path.DirectorySeparatorChar + Path.GetFileNameWithoutExtension(fileName) + ".png");
                Texture2D image = new Texture2D(2, 2);
                image.LoadImage(contentImage);
                image.wrapMode = TextureWrapMode.Clamp;
                models.Add(ModelFromJSON(content, image));
            }
        }
        ListModels();
    }

    void ReloadFromServer()
    {
        StartCoroutine(GetServerData());
    }

    IEnumerator GetServerData()
    {
        loading.SetActive(true);
        scrollList.SetActive(false);

        models = new List<ModelScriptableObject>();
        files = new List<string>();

        UnityWebRequest www = UnityWebRequest.Get(appConfig.appConfig.stockUrl+"/list.json");
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            // Show results as text
            Debug.Log(www.downloadHandler.text);

            JObject joList = JObject.Parse(www.downloadHandler.text);

            if (joList["version"]!=null)
            {
                int requiredVersion = joList["version"].Value<int>();
                if (int.Parse(Application.version.Replace(".",""))<requiredVersion)
                {
                    loading.SetActive(false);
                    newerVersionRequired.SetActive(true);
                    yield break;
                }
            }

            foreach (JToken file in joList["files"])
            {
                string path = appConfig.appConfig.stockUrl + "/" + file["file"].ToString();
                UnityWebRequest wwwFile = UnityWebRequest.Get(path + ".json");
                yield return wwwFile.SendWebRequest();
                if (wwwFile.isNetworkError || wwwFile.isHttpError)
                {
                    Debug.Log(wwwFile.error);
                }
                else
                {
                    // Show results as text
                    Debug.Log(wwwFile.downloadHandler.text);
                    models.Add(ModelFromJSON(wwwFile.downloadHandler.text, null, path + ".png"));
                    files.Add(path);
                }
            }
        }

        ListModels();

        loading.SetActive(false);
        scrollList.SetActive(true);
    }


    ModelScriptableObject ModelFromJSON(string content, Texture2D image,string imageUrl="")
    {
        var jo = JObject.Parse(content);
        string modelName = jo["model_name"].ToString();
        ModelScriptableObject modelOrig = appConfig.appConfig.modelList.Where(m => m.modelName == modelName).First();
        ModelScriptableObject model = Instantiate(modelOrig);
        model.configuredObject = jo;
        model.modelDescription = jo["description"].ToString();
        model.modelPrice = float.Parse(jo["price"].ToString());
        if (image != null)
            model.image = Sprite.Create(image, new Rect(0, 0, image.width, image.height), new Vector2(0.5f, 0.5f));
        else
            model.image = null;
        model.imageUrl = imageUrl;
        return model;
    }

    public void ListModels()
    {
        if (listEnabled == false)
            return;

        toggleFilterCornerIsOn=toggleFilterCorner.isOn;
        toggleFilterUIsOn=toggleFilterU.isOn;
        toggleFilter321IsOn=toggleFilter321.isOn;

        foreach (Transform child in modelGUIPrefab.transform.parent)
        {
            if (child != modelGUIPrefab.transform)
            {
                Destroy(child.gameObject);
            }
        }

        for(int i=0;i<models.Count;i++)
        {
            bool isCorner =Convert.ToBoolean( models[i].configuredObject["corner"]);
            bool isU = Convert.ToBoolean(models[i].configuredObject["u"]);
            bool is321 = Convert.ToBoolean(models[i].configuredObject["321"]);

            bool add = Convert.ToInt16(models[i].configuredObject["count"])>0 || appConfig.adminApp;
            if (toggleFilterCorner.isOn==false && isCorner)
            {
                add=false;
            }
            if (toggleFilterU.isOn==false && isU)
            {
                add = false;
            }
            if (toggleFilter321.isOn==false && is321)
            {
                add = false;
            }

            if (add)
            {
                ModelGUI newModelGUI = Instantiate(modelGUIPrefab, modelGUIPrefab.transform.parent);
                newModelGUI.gameObject.SetActive(true);
                newModelGUI.Init(models[i], files[i]);
            }
        }
        modelGUIPrefab.gameObject.SetActive(false);
    }
}
