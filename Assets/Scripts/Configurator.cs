﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Configurator : MonoBehaviour
{
    public static Configurator instance;

    public static ModelScriptableObject model;
    public ModelScriptableObject modelEditor;

    public AppConfigGlobalScriptableObject appConfigGlobal;

    public PartGUI partGUIPrefab;
    public MaterialGUI materialGUIPrefab;

    public Groups viewGroups;
    public Groups topViewFooterGroups;

    public GameObject topViewGroup;
    public GameObject threeDViewGroup;

    public GameObject topViewFooterPartsGroup;
    public GameObject materials;

    public PartPointGUI partPointGUI;

    public Transform configuredObjectParent;

    public Transform selectionGUI;

    public CameraController cameraController;

    public DimensionsGUI dimensionsGUI;

    public GameObject selectPart;

    public MaterialGUI materialLegGUIPrefab;
    public MaterialGUI typeLegGUIPrefab;
    public GameObject listLegMaterials;
    public GameObject listLegTypes;

    PartPoint attachToPartPoint;

    Part selectedPart;

    public List<Part> parts;
    List<PartGUI> partsGUI;

    public MaterialScriptableObject[] lastMaterial;
    public ColorSet[] lastColor;

    public MaterialScriptableObject[] lastMaterialLegs;
    public ColorSet[] lastColorLegs;
    public MaterialGUI lastTypeLegs;

    bool listedMaterials = false;
    bool materialsShowed = false;
    public GameObject materialButton;

    public Text textModel;
    public TMPro.TextMeshProUGUI textmpModel;

    public Button buttonAR;
    public Button buttonLegs;
    public Button buttonSecondColor;
    public Button buttonColor;
    public Button buttonDelete;
    public Button buttonSewing;
    public Button buttonShop;

    public Sprite spriteButtonNormal;
    public Sprite spriteButtonSelected;

    public GameObject adminGUI;
    public GameObject stockGUI;
    public Text textStockInfo;

    [SerializeField]
    public LocalizationManager.Localization[] localizationLocation;
    [SerializeField]
    public LocalizationManager.Localization[] localizationMaterial;
    [SerializeField]
    public LocalizationManager.Localization[] localizationPrice;
    [SerializeField]
    public LocalizationManager.Localization[] localizationColorMain;
    [SerializeField]
    public LocalizationManager.Localization[] localizationColorSecondary;

    public GameObject floorPlane;

    public MailSender mailSender;

    int materialIndex;

    int sewingIndex;

    int legTypeIndex;

    private List<PartPointGUI> partPointGUIs=new List<PartPointGUI>();

    private void Awake()
    {
        instance = this;

        Input.simulateMouseWithTouches = true;

        if (model == null)
            model = modelEditor;

        adminGUI.SetActive(appConfigGlobal.adminApp);

        partPointGUI.gameObject.SetActive(false);
        LoadModel();
        CalculateDimensions();
        selectionGUI.gameObject.SetActive(false);

        materialButton.SetActive(false);

        listLegMaterials.SetActive(false);

        ShowMaterials(false);

        buttonAR.gameObject.SetActive(Application.platform != RuntimePlatform.WebGLPlayer);

        if (buttonColor!=null)
            buttonColor.gameObject.SetActive(false);

        SetButtonSecondColor();
        SetButtonSewing();

        lastMaterial = new MaterialScriptableObject[model.materialCount];
        lastColor = new ColorSet[model.materialCount];

        lastMaterialLegs = new MaterialScriptableObject[model.legsCount];
        lastColorLegs = new ColorSet[model.legsCount];

        if (buttonShop != null)
            buttonShop.gameObject.SetActive(false);

        buttonLegs.gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        instance = null;
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            BackToMenu();
        }
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene(appConfigGlobal.appConfig.menuScene);
    }

    void LoadModel()
    {
        parts = new List<Part>();
        ListParts();
        ListLegMaterials();
        ListLegTypes();
        if (textModel != null)
            textModel.text = model.modelName;
        if (textmpModel != null)
            textmpModel.text = model.modelName;

        LoadConfiguredObject();
    }

    void ListParts()
    {
        partsGUI = new List<PartGUI>();

        //foreach (Part part in model.parts)
        foreach (Part part in AssetBundleManager.instace.parts)
        {
            PartGUI newPartGUI = Instantiate(partGUIPrefab, partGUIPrefab.transform.parent);
            newPartGUI.gameObject.SetActive(true);
            newPartGUI.Init(part);
            partsGUI.Add(newPartGUI);
        }

        partGUIPrefab.gameObject.SetActive(false);

        DisableUncompatibleParts();
    }

    public void ListMaterials()
    {
        ClearMaterialsList();
        foreach (MaterialScriptableObject material in model.materials.OrderBy(m=>m.materialName))
        {
            MaterialGUI newMaterialGUI = Instantiate(materialGUIPrefab, materialGUIPrefab.transform.parent);
            newMaterialGUI.gameObject.SetActive(true);

            if (lastColor[materialIndex]==null)
            {
                if (material.colors != null)
                    lastColor[materialIndex] = material.colors.colors[0];
            }

            string name = material.materialName;

            ColorSet colorSet = null;
            if (material.colors!=null)
            {
                colorSet = material.colors.colors[0];
            }
            newMaterialGUI.Init(material, colorSet, name);
        }
        materialGUIPrefab.gameObject.SetActive(false);
        listedMaterials = true;
    }

    void ListColors()
    {
        if (lastMaterial[materialIndex].colors == null)
            return;

        ClearMaterialsList();

        ColorSet[] colorSets = lastMaterial[materialIndex].colors.colors;
        if (lastMaterial[materialIndex].colors.orderByName)
        {
            colorSets = colorSets.OrderBy(cs => int.Parse(cs.colorName)).ToArray();
        }

        foreach (ColorSet colorSet in colorSets)
        {
            MaterialGUI newMaterialGUI = Instantiate(materialGUIPrefab, materialGUIPrefab.transform.parent);
            newMaterialGUI.gameObject.SetActive(true);

            string name = lastMaterial[materialIndex].materialName + " " + colorSet.colorName;

            newMaterialGUI.Init(lastMaterial[materialIndex], colorSet, name);
        }
        listedMaterials = false;

        materialButton.SetActive(true);
    }

    void ClearMaterialsList()
    {
        foreach (Transform t in materialGUIPrefab.transform.parent)
        {
            if (t != materialGUIPrefab.transform)
                Destroy(t.gameObject);
        }
    }

    void ClearLegsMaterialsList()
    {
        foreach (Transform t in materialLegGUIPrefab.transform.parent)
        {
            if (t != materialLegGUIPrefab.transform)
                Destroy(t.gameObject);
        }
    }

    public void SelectPartPoint(PartPointGUI partPointGUI)
    {
        attachToPartPoint = partPointGUI.partPoint;
        ShowParts();
    }

    private void ShowParts()
    {
        ShowMaterials(false);
        viewGroups.ActivateGroup(topViewGroup);
        //topViewFooterGroups.ActivateGroup(topViewFooterPartsGroup);
        topViewFooterPartsGroup.SetActive(true);
        DisableUncompatibleParts();
    }

    private void ShowMaterials(bool show)
    {
        if (show && materialsShowed == false)
        {
            materialsShowed = true;
            ListMaterials();
        }

        if (show)
        {
            topViewFooterPartsGroup.SetActive(false);
            ShowLegListMaterials(false);
        }
        
        materials.SetActive(show);
        SetButtonSecondColor();
        SetButtonSewing();

        if (buttonColor!=null && buttonSecondColor!=null)
        {
            if (show == false)
            {
                buttonColor.GetComponent<ButtonSelectable>().Select(false);
                buttonSecondColor.GetComponent<ButtonSelectable>().Select(false);
            }
        }
    }

    

    public void ToggleMaterials()
    {
        ShowMaterials(materials.activeInHierarchy == false);
    }

    void DisableUncompatibleParts()
    {
        foreach (PartGUI partGUI in partsGUI)
        {
            bool interactable = partGUI.part.canBeStandalone;
            if (attachToPartPoint != null)
                interactable = partGUI.part.CanConnectTo(attachToPartPoint, appConfigGlobal.appConfig.partPointConnections);

            if (interactable==false && appConfigGlobal.appConfig.hideUncompatibleParts)
            {
                partGUI.gameObject.SetActive(false);
            }
            else
            {
                partGUI.gameObject.SetActive(true);
                partGUI.SetInteractable(interactable);
            }
        }
    }

    public void AddPart(PartGUI part, PartOptionalFunctionScriptableObject optionalFunction)
    {
        AddPart(part.part, optionalFunction);
    }

    public void AddPart(Part part, PartOptionalFunctionScriptableObject optionalFunction)
    {
        Part newPart = Instantiate(part, configuredObjectParent);
        newPart.Init(model, optionalFunction);
        parts.Add(newPart);

        if (EditingEnabled())
            InitPartPoints(newPart);

        if (attachToPartPoint == null)
        {
            newPart.transform.localPosition = Vector3.zero;
        }
        else
        {
            SetPositionByPartPoint(newPart, attachToPartPoint);
        }
        //topViewFooterGroups.ActivateGroup(null);
        materials.SetActive(false);
        topViewFooterPartsGroup.SetActive(false);

        CalculateDimensions();

        selectPart.SetActive(false);

        for (int i = 0; i < model.materialCount; i++)
        {
            if (lastMaterial[i] != null)
            {
                SetMaterial(i, lastMaterial[i], lastColor[i]);
            }
            else
            {
                ColorSet colorSet = null;
                if (model.materials[model.materialDefaultIndex].colors != null)
                {
                    colorSet = model.materials[model.materialDefaultIndex].colors.colors[0];
                }
                SetMaterial(i, model.materials[model.materialDefaultIndex], colorSet, false);
            }
        }

        SetDefaultLegsMaterial();

        if (lastTypeLegs != null)
            SetTypeLegs(lastTypeLegs);

        if (model.sewingCount>1)
            newPart.SetSewing(sewingIndex);

        if (buttonColor!=null)
            buttonColor.gameObject.SetActive(true);

        SetButtonSecondColor();
    }


    void SetPositionByPartPoint(Part part, PartPoint partPointToConnect)
    {
        PartPointConnection connection = appConfigGlobal.appConfig.partPointConnections.Where(conn => conn.canConnectTo.Contains(partPointToConnect.connection)).First();
        PartPoint partPoint = part.partPoints.Where(partP => partP.connection == connection.connection && partP.type == part.type).First();
        float angle = SignedAngleBetween(partPoint.transform.right, partPointToConnect.transform.right, new Vector3(0, 1, 0)) + 180f;
        part.transform.RotateAround(partPoint.transform.localPosition, new Vector3(0, 1, 0), angle);
        Vector3 dir = partPointToConnect.transform.position - partPoint.transform.position;
        part.transform.position += dir;

        partPoint.ConnectTo(partPointToConnect);
        partPointToConnect.ConnectTo(partPoint);

        EnableDisablePartPoints();
    }

    void EnableDisablePartPoints()
    {
        // if there is taburet instantiated, disable all other taburet part points
        bool taburetPresent = partPointGUIs.Where(ppg => ppg.partPoint.type == 2 && ppg.partPoint.Connected).Count() > 0;
        foreach (PartPointGUI partPointGUI in partPointGUIs.Where(ppg => ppg.partPoint.type == 2))
        {
            partPointGUI.gameObject.SetActive(taburetPresent == false);
        }
    }

    float SignedAngleBetween(Vector3 a, Vector3 b, Vector3 n)
    {
        // angle in [0,180]
        float angle = Vector3.Angle(a, b);
        float sign = Mathf.Sign(Vector3.Dot(n, Vector3.Cross(a, b)));

        // angle in [-179,180]
        float signed_angle = angle * sign;

        // angle in [0,360] (not used but included here for completeness)
        //float angle360 =  (signed_angle + 180) % 360;

        return signed_angle;
    }

    void InitPartPoints(Part part)
    {
        if (part.canBeStandalone)
        {
            foreach (PartPoint partPoint in part.partPoints)
            {
                PartPointGUI newPartPointGUI = Instantiate(partPointGUI, partPointGUI.transform.parent);
                newPartPointGUI.gameObject.SetActive(true);
                newPartPointGUI.transform.SetAsFirstSibling();
                newPartPointGUI.Init(partPoint, appConfigGlobal.appConfig.partPointIcons[partPoint.type]);

                partPointGUIs.Add(newPartPointGUI);
            }
        }

        EnableDisablePartPoints();
    }

    public void SelectPart(Part part)
    {
        if (EditingEnabled() == false)
            return;

        selectedPart = part;

        if (parts.Count > 0 && topViewGroup.activeInHierarchy)
        {
            if (part == null)
            {
                Debug.Log("Part deselected");
                selectionGUI.gameObject.SetActive(false);
                ShowMaterials(false);
            }
            else
            {
                Debug.Log("Part selected:" + part.partName);

                selectionGUI.gameObject.SetActive(true);
                selectionGUI.GetComponent<FollowTransform>().follow = part.transform;

                ShowMaterials(true);
            }
        }
        else
        {
            selectionGUI.gameObject.SetActive(false);
            ShowMaterials(false);
        }

        DisableOptionalFunctionsGUI();
    }

    public void DisableOptionalFunctionsGUI(PartGUI except = null)
    {
        PartGUI[] partGuis = GameObject.FindObjectsOfType<PartGUI>();
        foreach (PartGUI partGui in partGuis)
        {
            if (partGui != except)
            {
                partGui.ShowOptionalFunctions(false);
            }
        }

    }

    public void DeletePart()
    {
        partPointGUIs.RemoveAll(ppg => ppg.partPoint.part == selectedPart);

        parts.Remove(selectedPart);
        selectedPart.Disconnect();
        for (int p = parts.Count - 1; p > -1; p--)
        {
            Part part = parts[p];
            if (part.Connected == false && part.canBeStandalone == false)
            {
                parts.Remove(part);
                Destroy(part.gameObject);
            }
        }
        Destroy(selectedPart.gameObject);
        SelectPart(null);
        CalculateDimensions();
        selectPart.SetActive(parts.Count == 0);
        if (parts.Count == 0)
        {
            attachToPartPoint = null;
            ShowParts();
        }

        if (buttonColor!=null)
            buttonColor.gameObject.SetActive(parts.Count>0);

        ShowMaterials(false);

        EnableDisablePartPoints();
    }

    void SetMaterial(int matIndex, MaterialScriptableObject material, ColorSet color, bool listColors=true)
    {
        Material m = material.material;
        Color c = Color.white;
        if (color != null)
        {
            if (color.material != null)
                m = color.material;
            c = color.color;
        }
        foreach (Part part in parts)
        {
            string matName = "main";
            if (matIndex > 0)
                matName += (matIndex + 1).ToString();
            part.SetMaterial(m, c, matName);
        }


        lastMaterial[matIndex] = material;
        lastColor[matIndex] = color;

        if (matIndex == 0 && appConfigGlobal.appConfig.allowMultipleMaterialTypes==false)
        {
            for (int i=1;i<model.materialCount;i++)
            {
                if (lastMaterial[i]!=material)
                {
                    SetMaterial(i, material, color, false);
                }
            }
        }

        if (listColors && listedMaterials)
        {
            ListColors();
        }

        SetSelectedMaterialButton();
    }

    public void SetMaterial(MaterialGUI materialGUI)
    {
        SetMaterial(materialIndex, materialGUI.material, materialGUI.colorSet);
    }

    public void SetMaterialLegs(MaterialGUI materialGUI)
    {
        SetMaterialLegs(materialGUI.material, materialGUI.colorSet);
    }

    void ListLegMaterials()
    {
        MaterialScriptableObject[] matList = model.materialsLegs.OrderBy(m=>m.materialName).ToArray();
        if (legTypeIndex>0 && model.materialsLegs2.Length>0)
        {
            matList = model.materialsLegs2.OrderBy(m => m.materialName).ToArray();
        }

        ClearLegsMaterialsList();

        foreach (MaterialScriptableObject mat in matList)
        {
            if (mat.colors != null)
            {
                foreach (ColorSet colorSet in mat.colors.colors.OrderBy(c=>c.colorName))
                {
                    MaterialGUI newMaterialGUI = Instantiate(materialLegGUIPrefab, materialLegGUIPrefab.transform.parent);
                    newMaterialGUI.gameObject.SetActive(true);
                    newMaterialGUI.Init(mat, colorSet, colorSet.colorName);
                }
            }
            else
            {
                MaterialGUI newMaterialGUI = Instantiate(materialLegGUIPrefab, materialLegGUIPrefab.transform.parent);
                newMaterialGUI.gameObject.SetActive(true);
                newMaterialGUI.Init(mat, null, mat.materialName);
            }
        }

        materialLegGUIPrefab.gameObject.SetActive(false);
    }

    void ListLegTypes()
    {
        if (typeLegGUIPrefab == null)
            return;

        if (model.legsCount>1)
        {
            for (int i = 0; i < model.legsCount; i++)
            {
                MaterialGUI newTypeGUI = Instantiate(typeLegGUIPrefab, typeLegGUIPrefab.transform.parent);
                newTypeGUI.gameObject.SetActive(true);
                newTypeGUI.Init(model.spriteLegs[i], "Typ " + (i + 1).ToString(), i);
            }
        }

        typeLegGUIPrefab.gameObject.SetActive(false);

        listLegTypes.SetActive(model.legsCount > 1);
    }

    void SetMaterialLegs(MaterialScriptableObject material, ColorSet color)
    {
        lastMaterialLegs[legTypeIndex] = material;
        lastColorLegs[legTypeIndex] = color;

        foreach (Part part in parts)
        {
            for (int i=-1;i<model.legsCount;i++)
            {
                string group = "legs";
                if (i!=0)
                {
                    group += (i + 1).ToString();
                }
                part.SetMaterial(material.material, color.color, group);
                if (material.materialShelf != null)
                    part.SetMaterial(material.materialShelf, color.color, "shelf");
            }
        }

        //listLegMaterials.SetActive(false);
    }

    public void SetTypeLegs(MaterialGUI materialGUI)
    {
        lastTypeLegs = materialGUI;
        legTypeIndex = materialGUI.index;

        string enableType = "legs";
        if (materialGUI.index>0)
        {
            enableType += (materialGUI.index + 1).ToString();
        }
        foreach (Part part in parts)
        {
            PartMeshType[] partMeshTypes = part.GetComponentsInChildren<PartMeshType>(true);
            foreach(PartMeshType partMeshType in partMeshTypes)
            {
                if (partMeshType.partMeshType.StartsWith("legs") && partMeshType.partMeshType!="legs0")
                {
                    partMeshType.gameObject.SetActive(partMeshType.partMeshType == enableType);
                }
            }
        }

        SetDefaultLegsMaterial();

        ListLegMaterials();
    }

    private void SetDefaultLegsMaterial()
    {
        MaterialScriptableObject[] matList = model.materialsLegs;
        if (legTypeIndex > 0 && model.materialsLegs2.Length > 0)
        {
            matList = model.materialsLegs2;
        }

        if (lastMaterialLegs[legTypeIndex] != null)
            SetMaterialLegs(lastMaterialLegs[legTypeIndex], lastColorLegs[legTypeIndex]);
        else
            SetMaterialLegs(matList[0], matList[0].colors.colors[0]);
    }

    public void ToggleLegListMaterials()
    {
        ShowLegListMaterials(listLegMaterials.activeInHierarchy == false);
    }

    public void ShowLegListMaterials(bool show)
    {
        listLegMaterials.SetActive(show);

        if (show)
            ShowMaterials(false);
        else
        {
            ButtonSelectable selectable = buttonLegs.GetComponent<ButtonSelectable>();
            if (selectable != null)
                selectable.Select(false, false);
        }
    }

    public void SwitchTo3DView()
    {
        viewGroups.ActivateGroup(threeDViewGroup);
        RecenterObject();
        cameraController.SwitchTo3DView();
        dimensionsGUI.Hide();

        floorPlane.SetActive(true);
        buttonDelete.gameObject.SetActive(false);

        buttonLegs.gameObject.SetActive(true);

        if (buttonShop != null)
            buttonShop.gameObject.SetActive(true);
    }

    public void SwitchTo2DView()
    {
        viewGroups.ActivateGroup(topViewGroup);
        cameraController.SwitchTo2DView();
        CalculateDimensions();

        if (Application.platform == RuntimePlatform.WebGLPlayer)
            floorPlane.SetActive(false);

        buttonDelete.gameObject.SetActive(true);

        buttonLegs.gameObject.SetActive(false);

        if (buttonShop != null)
            buttonShop.gameObject.SetActive(false);

        ShowLegListMaterials(false);
    }

    public void SwitchToARView()
    {
        StartCoroutine(SwitchToARViewCoroutine());
    }

    IEnumerator SwitchToARViewCoroutine()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("ar", LoadSceneMode.Additive);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }

        Scene sceneAR = SceneManager.GetSceneByName("ar");
        SceneManager.SetActiveScene(sceneAR);

        ARManager arManager = GameObject.FindObjectOfType<ARManager>();
        arManager.DisableConfigurator();
    }

    void CalculateDimensions()
    {
        if (model.configuredObject != null)
            return;

        Bounds bounds = GetBoundsDimensions();
        dimensionsGUI.SetDimensions(bounds.center, bounds.size.x, bounds.size.z, 0f);
    }

    Bounds GetBoundsMesh()
    {
        Bounds bounds = new Bounds();

        foreach (Part part in parts)
        {
            if (part.includeInDimensions) 
                bounds.Encapsulate(part.PartBoundsMesh());
        }

        return bounds;
    }

    Bounds GetBoundsDimensions()
    {
        Bounds b = new Bounds(Vector3.zero,Vector3.zero);

        foreach (Part part in parts)
        {
            if (part.includeInDimensions)
            {
                Bounds partBoundsDimensions = part.PartBoundsDimensions();
                if (b.size == Vector3.zero)
                {
                    b = partBoundsDimensions;
                }
                else
                {
                    b.Encapsulate(partBoundsDimensions);
                }
            }
        }

        Bounds bounds = new Bounds(GetBoundsMesh().center, b.size);

        return bounds;
    }

    void RecenterObject()
    {
        Vector3 center = GetBoundsMesh().center;
        center.y = 0;

        foreach (Transform child in configuredObjectParent)
            child.localPosition -= center;
    }

    public string GetMaterialName()
    {
        string m = "";
        for (int i = 0; i < model.materialCount; i++)
        {
            if (m.Length > 0)
                m += ", ";
            m += lastMaterial[i].materialName + " " + lastColor[i].colorName;
        }
        return m;
    }

    void LoadConfiguredObject()
    {
        stockGUI.SetActive(model.configuredObject != null);
        buttonLegs.gameObject.SetActive(model.configuredObject == null);

        if (model.configuredObject != null)
        {
            Debug.Log("Constructing " + model.configuredObject.ToString());
            foreach (JToken part in model.configuredObject["parts"].ToList())
            {
                Debug.Log("Creating part " + part["name"].ToString());
                Part p = AssetBundleManager.instace.parts.Where(pp => pp.name.Equals(part["name"].ToString())).First();

                foreach (JToken connection in part["connections"])
                {
                    string partPointId = connection["part_point"].ToString();
                    int connectToIndex = connection["connected_to_part"].Value<int>();
                    string connectToPart = connection["connected_to_part_point"].ToString();
                    PartPoint partPoint = p.partPoints.Where(pp => pp.connection.Equals(partPointId)).First();

                    if (connectToIndex < parts.Count)
                    {
                        selectedPart = parts[connectToIndex];
                        attachToPartPoint = selectedPart.partPoints.Where(pp => pp.connection.Equals(connectToPart) && pp.type.Equals(partPoint.type) && pp.connectedTo == null).First();
                        break;
                    }
                }

                AddPart(p, null);
            }

            RecenterObject();

            string stockInfo = string.Format(LocalizationManager.GetLocalizedText(localizationMaterial) + ": {0}" + Environment.NewLine + LocalizationManager.GetLocalizedText(localizationLocation) + ": {1}" + Environment.NewLine + LocalizationManager.GetLocalizedText(localizationPrice) + ": {2}", model.configuredObject["description_material"].ToString(), model.configuredObject["location"].ToString(), model.configuredObject["price"].ToString() + " €");
            textStockInfo.text = stockInfo;

            float width = model.configuredObject["dimensions"]["width"].Value<float>() / 100f;

            float depth = 0f;
            if (model.configuredObject["dimensions"]["depth"] != null)
            {
                string depthString = model.configuredObject["dimensions"]["depth"].ToString();
                if (depthString.Length > 0)
                    depth = float.Parse(depthString) / 100f;
            }

            float depthLeft = 0f;
            if (model.configuredObject["dimensions"]["depth2"] != null)
            {
                string depthLeftString = model.configuredObject["dimensions"]["depth2"].ToString();
                if (depthLeftString.Length > 0)
                    depthLeft = float.Parse(depthLeftString) / 100f;
            }

            dimensionsGUI.SetDimensions(Vector3.zero, width, depth, depthLeft);

            string materialName = model.configuredObject["material"].ToString();
            var materials = model.materials.Where(m => m.materialName.Equals(materialName));
            if (materials.Count() > 0)
            {
                MaterialScriptableObject material = materials.First();
                ColorSet colorSet = material.colors.colors.Where(c => c.colorName.Equals(model.configuredObject["material_color"].ToString())).First();
                SetMaterial(0, material, colorSet);
            }
            else
            {
                Debug.LogError("Material " + materialName + " not found!");
            }

            string materialNameLegs = model.configuredObject["material_legs"].ToString();
            var materialsLegs = model.materialsLegs.Where(m => m.materialName.Equals(materialNameLegs));
            if (materialsLegs.Count() > 0)
            {
                MaterialScriptableObject materialLegs = materialsLegs.First();
                ColorSet colorSetLegs = materialLegs.colors.colors.Where(c => c.colorName.Equals(model.configuredObject["material_legs_color"].ToString())).First();
                SetMaterialLegs(materialLegs, colorSetLegs);
            }
            else
            {
                Debug.LogError("Legs Material " + materialNameLegs + " not found!");
            }
        }
    }

    public bool EditingEnabled()
    {
        return model.configuredObject == null || appConfigGlobal.adminApp;
    }

    public void ButtonColor()
    {
        materialIndex = 0;

        ShowMaterials(true);

        SetSelectedMaterialButton();
    }

    public void ButtonSecondColor()
    {
        materialIndex = 1;

        ShowMaterials(true);

        if (appConfigGlobal.appConfig.allowMultipleMaterialTypes)
            ListMaterials();
        else
            ListColors();

        SetSelectedMaterialButton();


        /*
        materialIndex++;
        if (materialIndex >= model.materialCount)
            materialIndex = 0;

        SetButtonSecondColor();

        if (materialIndex == 0 || appConfigGlobal.appConfig.allowMultipleMaterialTypes)
            ListMaterials();
        else
            ListColors();

        SetSelectedMaterialButton();
        */
    }

    public void ButtonLegs()
    {
        ToggleLegListMaterials();
    }

    void SetButtonSecondColor()
    {
        if (buttonSecondColor==null)
        {
            return;
        }

        buttonSecondColor.gameObject.SetActive(model.materialCount > 1 && parts.Count>0);

        /*
        buttonSecondColor.gameObject.SetActive(materials.activeInHierarchy && model.materialCount > 1); 
        
        if (model.materialCount > 1)
        {
            string caption = LocalizationManager.GetLocalizedText(localizationColorMain);
            if (materialIndex>0)
            {
                caption = LocalizationManager.GetLocalizedText(localizationColorSecondary);
                if (model.materialCount>2)
                {
                    caption += " " + (materialIndex);
                }
            }
            buttonSecondColor.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = caption;
        }
        */
    }

    public void ButtonSewing()
    {
        sewingIndex++;
        if (sewingIndex >= model.sewingCount)
            sewingIndex = 0;

        foreach(Part part in parts)
        {
            part.SetSewing(sewingIndex);
        }

        SetButtonSewing();
    }

    void SetButtonSewing()
    {
        if (buttonSewing == null)
            return;

        buttonSewing.gameObject.SetActive(materials.activeInHierarchy && model.sewingCount > 1);
    }

    void SetSelectedMaterialButton()
    {
        if (lastMaterial[materialIndex]!=null && lastColor[materialIndex] !=null)
        {
            string name = lastMaterial[materialIndex].materialName + " " + lastColor[materialIndex].colorName;
            materialButton.GetComponent<MaterialGUI>().Init(lastMaterial[materialIndex], lastColor[materialIndex], name);
        }
        else
        {
            materialButton.SetActive(false);
        }

        materialButton.GetComponent<Button>().interactable = materialIndex == 0 || appConfigGlobal.appConfig.allowMultipleMaterialTypes;
    }

    public void EnablePartType(int type, bool enable)
    {
        foreach(Part part in parts.Where(p=>p.type==type))
        {
            part.gameObject.SetActive(enable);
        }
    }

    public void SetOut(SetOut setOut)
    {
        SetOut[] setOuts = GameObject.FindObjectsOfType<SetOut>();
        foreach(SetOut so in setOuts)
        {
            if (so!=setOut)
            {
                ToggleOnClick toggleOnClick = so.GetComponentInChildren<ToggleOnClick>();
                if (toggleOnClick!=null)
                {
                    toggleOnClick.Toggle();
                }
            }
        }
    }
}
