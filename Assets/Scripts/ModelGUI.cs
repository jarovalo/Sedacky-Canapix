﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ModelGUI : MonoBehaviour
{
    public Image image;
    public Text text;
    public TMPro.TextMeshProUGUI textmeshproText;
    public Text textPrice;

    public GameObject variants;

    public Groups groups;
    public GameObject groupLoading;

    string fileName;

    ModelScriptableObject model;

    public void Init(ModelScriptableObject model, string fileName = "")
    {
        this.model = model;
        this.fileName = fileName;

        image.sprite = model.image;
        image.preserveAspect = true;

        if (text!=null)
            text.text = model.modelName;

        if (textmeshproText != null)
            textmeshproText.text = model.modelName;

        if (textPrice != null)
        {
            if (model.modelPrice > 0)
                textPrice.text = model.modelPrice.ToString() + " €";
            else
                textPrice.text = "";
        }

    }

    private void Start()
    {
        if (model.image == null && model.imageUrl != null)
        {
            this.image.gameObject.SetActive(false);
            StartCoroutine(LoadImageFromUrl());
        }
    }

    public void OnClick()
    {
        if (model.variants.Length>0)
        {
            variants.SetActive(true);
        }
        else
        {
            groups.ActivateGroup(groupLoading);
            SaveManager.fileName = fileName;
            AssetBundleManager.instace.LoadModelAssetBundle(model);
        }
    }

    public void OnClickVariant(int index)
    {
        SaveManager.fileName = fileName;
        AssetBundleManager.instace.LoadModelAssetBundle(model.variants[index]);
    }

    IEnumerator LoadImageFromUrl()
    {
        UnityWebRequest wwwTexture = UnityWebRequestTexture.GetTexture(model.imageUrl);
        yield return wwwTexture.SendWebRequest();
        if (wwwTexture.isNetworkError || wwwTexture.isHttpError)
        {
            Debug.Log(wwwTexture.error);
        }
        else
        {
            Texture2D image = ((DownloadHandlerTexture)wwwTexture.downloadHandler).texture;
            image.wrapMode = TextureWrapMode.Clamp;
            model.image = Sprite.Create(image, new Rect(0, 0, image.width, image.height), new Vector2(0.5f, 0.5f));
            this.image.sprite = model.image;
            this.image.gameObject.SetActive(true);
        }
    }

    private void OnDestroy()
    {
        StopCoroutine(LoadImageFromUrl());
    }
}
