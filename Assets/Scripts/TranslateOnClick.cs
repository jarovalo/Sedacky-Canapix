﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class TranslateOnClick : MonoBehaviour
{
    public Vector3 LocalPosOriginal;
    public Vector3 LocalPosTranslated;

    public UnityEvent OnTranslated;
    public UnityEvent OnReturned;

    private void OnMouseUp()
    {
        if (CameraController.IsCamera2D)
            return;

        if (this.enabled==false)
            return;

        if (transform.localPosition == LocalPosOriginal)
        {
            transform.localPosition = LocalPosTranslated;
            OnTranslated.Invoke();
        }
        else
        {
            transform.localPosition = LocalPosOriginal;
            OnReturned.Invoke();
        }
    }

    private void OnEnable()
    {
        
    }
}
