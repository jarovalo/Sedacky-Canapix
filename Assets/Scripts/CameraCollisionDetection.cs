﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCollisionDetection : MonoBehaviour
{
    public LayerMask DetectCollisionLayers;

    bl_CameraOrbit cameraOrbit;
    Vector3 lastPosition;
    Quaternion lastRotation;

    float lastDistance;
    float CollisionRadius = 0.3f;

    private void Awake()
    {
        cameraOrbit = GetComponent<bl_CameraOrbit>();
    }

    private void LateUpdate()
    {
        Vector3 forward = cameraOrbit.Target.position- cameraOrbit.transform.position;
        //create a ray from transform to target
        RaycastHit hit;
        //if ray detect a an obstacle in between the point of origin and the target
        if (Physics.SphereCast(transform.position, CollisionRadius, forward, out hit, 0.1f, DetectCollisionLayers))
        {
            /*
            cameraOrbit.transform.position = lastPosition;
            cameraOrbit.transform.rotation = lastRotation;
            cameraOrbit.Distance = lastDistance;
            */

            cameraOrbit.ZoomControll(true, -0.1f);
            //Debug.Log("hit");
        }

        lastPosition = cameraOrbit.transform.position;
        lastRotation = cameraOrbit.transform.rotation;
        lastDistance = cameraOrbit.Distance;
    }
}
