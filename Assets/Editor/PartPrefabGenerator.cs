﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class PartPrefabGenerator : EditorWindow
{
    ModelScriptableObject model;
    GameObject partMesh;
    Vector3 dimensions;
    Sprite image;
    bool canBeStandalone = true;
    GameObject dummyPointLeft;
    GameObject dummyPointRight;
    GameObject[] dummyPointsType1=new GameObject[3];
    GameObject[] partMeshesMain = new GameObject[30];
    GameObject[] partMeshesLegs = new GameObject[10];

    // Add menu named "My Window" to the Window menu
    [MenuItem("Yahart/Part Prefab Generator")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        PartPrefabGenerator window = (PartPrefabGenerator)EditorWindow.GetWindow(typeof(PartPrefabGenerator));
        window.Show();
    }

    void OnGUI()
    {
        model=(ModelScriptableObject) EditorGUILayout.ObjectField("Model", model,typeof(ModelScriptableObject));
        partMesh =(GameObject) EditorGUILayout.ObjectField("Mesh", partMesh, typeof(GameObject));

        if (GUILayout.Button("Detect values"))
        {
            ClearValues();
            Bounds bounds=new Bounds();
            foreach (Transform t in partMesh.GetComponentsInChildren<Transform>())
            {
                Renderer r = t.GetComponent<Renderer>();
                if (r != null)
                {
                    bounds.Encapsulate(r.bounds);
                    //bool main = ( r.sharedMaterial.name.ToLower().Contains("potah") || r.sharedMaterial.name.ToLower().Contains("testing")) r.sharedMaterial.name.ToLower().Contains("drevo")==false;
                    bool main = r.sharedMaterial.name.ToLower().Contains("drevo") == false && 
                        r.gameObject.name.ToLower().Contains("nozicka")==false;
                    if (main)
                    {
                        AddArrayItem(partMeshesMain, t.gameObject);
                    }
                    else
                    {
                        AddArrayItem(partMeshesLegs, t.gameObject);
                    }
                }
                else
                {
                    if (t.name.ToLower().Contains("dummy"))
                    {
                        if (t.localPosition.x>0)
                        {
                            dummyPointLeft = t.gameObject;
                        }
                        else
                        {
                            dummyPointRight = t.gameObject;
                        }
                    }
                    /*else
                    {
                        AddArrayItem(dummyPointsType1, t.gameObject);
                    }*/
                }
            }
            dimensions = bounds.size;
        }

        dimensions = EditorGUILayout.Vector3Field("Dimensions", dimensions);
        image = (Sprite)EditorGUILayout.ObjectField("Image", image, typeof(Sprite));
        canBeStandalone = EditorGUILayout.Toggle("Can be standalone",canBeStandalone);
        dummyPointLeft =(GameObject) EditorGUILayout.ObjectField("Dummy Point Left", dummyPointLeft, typeof(GameObject));
        dummyPointRight = (GameObject)EditorGUILayout.ObjectField("Dummy Point Right", dummyPointRight, typeof(GameObject));

        EditorGUILayout.Separator();
        for (int i=0;i<dummyPointsType1.Length;i++)
        {
            dummyPointsType1[i] = (GameObject)EditorGUILayout.ObjectField("Dummy Point Type 1", dummyPointsType1[i], typeof(GameObject));
        }

        EditorGUILayout.Separator();
        for (int i = 0; i < partMeshesMain.Length; i++)
        {
            partMeshesMain[i] = (GameObject)EditorGUILayout.ObjectField("Part Mesh Main", partMeshesMain[i], typeof(GameObject));
        }

        EditorGUILayout.Separator();
        for (int i = 0; i < partMeshesLegs.Length; i++)
        {
            partMeshesLegs[i] = (GameObject)EditorGUILayout.ObjectField("Part Mesh Legs", partMeshesLegs[i], typeof(GameObject));
        }


        if (GUILayout.Button("Generate"))
        {
            string path =System.IO.Path.GetDirectoryName( AssetDatabase.GetAssetPath(model))+"/"+model.modelName+"/Parts/"+partMesh.name+".prefab";
            GameObject newPart = new GameObject();
            GameObject mesh = PrefabUtility.InstantiatePrefab(partMesh,newPart.transform) as GameObject;

            Part part=newPart.AddComponent<Part>();
            part.partName = partMesh.name;
            part.dimensions = dimensions;
            part.image = image;
            part.canBeStandalone = canBeStandalone;


            part.partPoints = new PartPoint[0];
            if (dummyPointLeft!=null)
            {
                GameObject dummyPointLeftInstance = mesh.transform.FindDeepChild(dummyPointLeft.name).gameObject;
                AddPartPoint(dummyPointLeftInstance, "l",0, part);
            }
            if (dummyPointRight != null)
            {
                GameObject dummyPointRightInstance = mesh.transform.FindDeepChild(dummyPointRight.name).gameObject;
                AddPartPoint(dummyPointRightInstance, "r",0, part);
            }
            for (int i = 0; i < dummyPointsType1.Length; i++)
            {
                if (dummyPointsType1[i] != null)
                {
                    GameObject dummyPointType1Instance = mesh.transform.FindDeepChild(dummyPointsType1[i].name).gameObject;
                    AddPartPoint(dummyPointType1Instance, "l",1, part);
                }
            }
            for (int i = 0; i < partMeshesMain.Length; i++)
            {
                if (partMeshesMain[i] != null)
                {
                    GameObject partMeshMainInstance = mesh.transform.FindDeepChild(partMeshesMain[i].name).gameObject;
                    PartMeshType partMeshType = partMeshMainInstance.AddComponent<PartMeshType>();
                    partMeshType.partMeshType = "main";
                    partMeshMainInstance.AddComponent<MeshCollider>();
                }
            }
            for (int i = 0; i < partMeshesLegs.Length; i++)
            {
                if (partMeshesLegs[i] != null)
                {
                    GameObject partMeshLegsInstance = mesh.transform.FindDeepChild(partMeshesLegs[i].name).gameObject;
                    PartMeshType partMeshType = partMeshLegsInstance.AddComponent<PartMeshType>();
                    partMeshType.partMeshType = "legs";
                }
            }

            newPart.layer = LayerMask.NameToLayer("Part");
            System.Array.ForEach(newPart.GetComponentsInChildren<Transform>(), (entry) => entry.gameObject.layer = LayerMask.NameToLayer("Part"));

            newPart.transform.eulerAngles = new Vector3(0, 180f, 0);
            PrefabUtility.SaveAsPrefabAsset(newPart,path);
        }
        if (GUILayout.Button("Clear values"))
        {
            ClearValues();
        }
    }

    void AddPartPoint(GameObject addTo,string connection,int partType,Part part)
    {
        PartPoint partPoint = addTo.AddComponent<PartPoint>();
        partPoint.connection = connection;
        partPoint.type = partType;
        System.Array.Resize(ref part.partPoints, part.partPoints.Length + 1);
        part.partPoints[part.partPoints.Length - 1] = partPoint;
    }

    void AddArrayItem(GameObject[] array,GameObject item)
    {
        for (int i = 0; i < array.Length; i++)
        {
            if (array[i] == null)
            {
                array[i] = item;
                break;
            }
        }
    }

    void ClearValues()
    {
        dimensions=Vector3.zero;
        image=null;
        canBeStandalone = true;
        dummyPointLeft=null;
        dummyPointRight=null;
        dummyPointsType1 = new GameObject[3];
        partMeshesMain = new GameObject[30];
        partMeshesLegs = new GameObject[10];
    }
}