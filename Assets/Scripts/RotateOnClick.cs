﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class RotateOnClick : MonoBehaviour
{
    public Vector3 LocalRotOriginal;
    public Vector3 LocalRotModified;

    public UnityEvent OnRotated;
    public UnityEvent OnReturned;

    private void OnMouseUp()
    {
        if (CameraController.IsCamera2D)
            return;

        if (this.enabled == false)
            return;

        if (transform.localEulerAngles==LocalRotOriginal)
        {
            transform.localEulerAngles = LocalRotModified;
            OnRotated.Invoke();
        }
        else
        {
            transform.localEulerAngles = LocalRotOriginal;
            OnReturned.Invoke();
        }
    }

    private void OnEnable()
    {
        
    }
}
