﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ColorSet
{
    public Color color=Color.white;
    public Material material;
    public Sprite sprite;
    public string colorName;
}
