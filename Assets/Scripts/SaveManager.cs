﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class SaveManager : MonoBehaviour
{
    public Configurator configurator;

    public InputField textDescription;
    public InputField textMaterial;
    public InputField textPrice;
    public InputField textCount;
    public InputField textDimensionsWidth;
    public InputField textDimensionsDepth;
    public InputField textDimensionsDepthLeft;
    public Dropdown dropdownLocation;
    public Toggle toggleCorner;
    public Toggle toggleU;
    public Toggle toggle321;

    public Text textStatus;

    public Screenshot screenshot;

    public static string fileName="";

    public GameObject buttonDelete;

    private void OnEnable()
    {
        screenshot.ScreenshotsTaken += ScreenshotTaken;
    }

    private void OnDisable()
    {
        screenshot.ScreenshotsTaken -= ScreenshotTaken;
    }

    private void Start()
    {
        textDescription.text = Configurator.model.modelName;
        buttonDelete.SetActive(fileName != "");
    }

    public void Save()
    {
        if (textDescription.text.Length == 0)
        {
            textStatus.text = "Zadajte označenie!";
            return;
        }
        if (textDimensionsDepth.text.Length==0 && textDimensionsDepthLeft.text.Length==0)
        {
            textStatus.text = "Zadajte min. jednu dĺžku!";
            return;
        }
        if (textDimensionsWidth.text.Length == 0)
        {
            textStatus.text = "Zadajte šírku!";
            return;
        }

        GameObject configuredObject = GameObject.FindGameObjectWithTag("ConfiguredObject");

        var jo = new JObject();
        jo.Add("model_name", Configurator.model.modelName);
        jo.Add("description", textDescription.text);
        jo.Add("description_material", textMaterial.text);
        jo.Add("price", textPrice.text);
        jo.Add("count", textCount.text);
        var joDimensions = new JObject();
        joDimensions.Add("width", textDimensionsWidth.text);
        joDimensions.Add("depth", textDimensionsDepth.text);
        joDimensions.Add("depth2", textDimensionsDepthLeft.text);
        jo.Add("dimensions", joDimensions);
        jo.Add("location", dropdownLocation.options[dropdownLocation.value].text);
        jo.Add("corner", toggleCorner.isOn);
        jo.Add("u", toggleU.isOn);
        jo.Add("321", toggle321.isOn);
        jo.Add("material", configurator.lastMaterial[0].materialName);
        jo.Add("material_color", configurator.lastColor[0].colorName);
        jo.Add("material_legs", configurator.lastMaterialLegs[0].materialName);
        jo.Add("material_legs_color", configurator.lastColorLegs[0].colorName);

        var jparts = new JArray();
        Part[] parts = configuredObject.GetComponentsInChildren<Part>();
        foreach (Part part in parts)
        {
            jparts.Add(PartToJObject(part));
        }
        jo.Add("parts", jparts);

        bool saveList = fileName == "";
        if (fileName == "")
        {
            fileName = DirPath() + Path.DirectorySeparatorChar + id;
        }

        File.WriteAllText(fileName + ".json", jo.ToString());

        textStatus.text = "Zostava bola úspešne uložená do súboru: " + fileName;

        Debug.Log(jo.ToString());

        screenshot.TakeScreenshots();

        buttonDelete.SetActive(true);

        if (saveList)
            SaveList();
    }

    public void Delete()
    {
        File.Delete(fileName + ".json");
        File.Delete(fileName + ".png");
        SaveList();
        UnityEngine.SceneManagement.SceneManager.LoadScene("menu");
    }

    public JObject PartToJObject(Part part)
    {
        var jo = new JObject();
        jo.Add("name", part.name.Replace("(Clone)",""));
        JArray connections = new JArray();
        for(int i=0;i< part.partPoints.Length;i++)
        {
            PartPoint partPoint = part.partPoints[i];
            if (partPoint.Connected)
            {
                JObject joConnection = new JObject();
                joConnection.Add("part_point", partPoint.connection);
                joConnection.Add("connected_to_part", configurator.parts.IndexOf(partPoint.connectedTo.part));
                joConnection.Add("connected_to_part_point", partPoint.connectedTo.connection);
                connections.Add(joConnection);
            }
        }
        jo.Add("connections", connections);
        return jo;
    }

    public string id
    {
        get
        {
            string uniqueId= textDescription.text.ToLower().Replace(" ", "")+DateTime.Now.ToString(  "yyyyMMddHHmmss");
            return uniqueId;
        }
    }


    void ScreenshotTaken(Texture2D[] textures)
    {
        byte[] bytes = textures[0].EncodeToPNG();
        File.WriteAllBytes(fileName + ".png", bytes);
    }

    void SaveList()
    {
        string listFile = DirPath() + Path.DirectorySeparatorChar + "list.json";
        string[] files = Directory.GetFiles(DirPath(), "*.json");
        var jo = new JObject();
        var jaFiles = new JArray();
        foreach(string file in files)
        {
            if (file!=listFile)
            {
                var joFile = new JObject();
                joFile.Add("file", Path.GetFileNameWithoutExtension( file));
                jaFiles.Add(joFile);
            }
        }
        jo.Add("files",jaFiles);
        File.WriteAllText(listFile, jo.ToString());
        Debug.Log("JSON list saved to file: " + listFile);
    }

    public static string DirPath()
    {
            string path= Application.persistentDataPath + Path.DirectorySeparatorChar + "stock";
            if (Directory.Exists(path)==false)
            {
                Directory.CreateDirectory(path);
            }
            return path;
    }
}
