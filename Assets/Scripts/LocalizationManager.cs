﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;

public class LocalizationManager : MonoBehaviour {
    [Serializable]
    public struct Localization
    {
        public string language;
        public string text;

        public Localization(KeyValuePair<string,string> kvp)
        {
            language = kvp.Key;
            text = kvp.Value;
        }
    }

    public static string[] availableLanguages = new string[] { "sk", "en" };

    //hardcoded lang
    static string lang="sk";

    public static string language
    {
        get
        {
            if (lang=="")
            {
                string defaultLanguage=GetCultureDefualt(Application.systemLanguage).TwoLetterISOLanguageName;
                if (availableLanguages.Contains(defaultLanguage)==false)
                    defaultLanguage = "en";
                lang = PlayerPrefs.GetString("language", defaultLanguage);
            }
            return lang;
        }
        set
        {
            lang = value;
            PlayerPrefs.SetString("language", language);
            LocalizedText[] localizedTexts = GameObject.FindObjectsOfType<LocalizedText>();
            foreach (LocalizedText localizedText in localizedTexts)
            {
                localizedText.Translate();
            }
        }
    }

    private static CultureInfo GetCultureDefualt(SystemLanguage language)
    {
        return CultureInfo.GetCultures(CultureTypes.AllCultures).
            FirstOrDefault(x => x.EnglishName == Enum.GetName(language.GetType(), language));
    }

    public static Dictionary<string,string> LocalizationsToDictionary(Localization[] localizations)
    {
        Dictionary<string, string> dic = new Dictionary<string, string>();
        foreach(Localization localization in localizations)
        {
            dic.Add(localization.language, localization.text);
        }
        return dic;
    }

    public static string GetLocalizedText(Dictionary<string, string> localizations)
    {
        if (localizations.ContainsKey(language))
            return localizations[language];
        else if (localizations.ContainsKey("en"))
            return localizations["en"];
        else if (localizations.Count > 0)
            return localizations.Values.ToList().First();
        else
            return "[missing text]";
    }

    public static string GetLocalizedText(Localization[] localizations)
    {
        return GetLocalizedText(LocalizationsToDictionary(localizations));
    }
}
