﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartMeshType : MonoBehaviour
{
    public string partMeshType="main";
    public int setMaterialIndex = -1;
    public int setMaterialIndex2 = -1;
    public bool sewing = false;

    Renderer rend;

    public void SetMaterial(Material material, Color color)
    {
        DetectRenderer();

        Material m;

        Material[] materials = rend.materials;
        m = Instantiate(material);

        if (Application.isPlaying==false)
        {
            materials = rend.sharedMaterials;
            m = material;
        }

        if (setMaterialIndex == -1)
        {
            for (int i = 0; i < materials.Length; i++)
            {
                materials[i] = m;
                materials[i].color = color;
            }
        }
        else
        {
            materials[setMaterialIndex] = m;
            materials[setMaterialIndex].color = color;

            if (setMaterialIndex2>-1)
            {
                materials[setMaterialIndex2] = m;
                materials[setMaterialIndex2].color = color;
            }
        }

        if (Application.isPlaying == false)
        {
            rend.sharedMaterials = materials;
        }
        else
        {
            rend.materials = materials;
        }
    }

    public void SetSecondaryNormal(Texture normal)
    {
        Material m = rend.materials[0];
        if (setMaterialIndex > -1)
        {
            m = rend.materials[setMaterialIndex];
        }

        m.SetTexture("_DetailNormalMap", normal);
        m.SetTextureScale("_DetailAlbedoMap", Vector2.one);
        m.SetFloat("_UVSec", 1);
        m.EnableKeyword("_DETAIL_MULX2");
    }

    void DetectRenderer()
    {
        if (rend==null)
            rend = GetComponent<Renderer>();
    }
}
