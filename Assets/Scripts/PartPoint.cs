﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class PartPoint : MonoBehaviour
{
    public int type=0;
    public string connection = "";

    public string[] allowedCategories;

    public string[] disallowedCategories;

    [HideInInspector]
    public PartPointGUI partPointGUI;

    public GameObject[] activateOnDisconnect;

    public PartPoint connectedTo;

    public Part part;

    private void Awake()
    {
        part = GetComponentInParent<Part>();
    }

    public void ConnectTo(PartPoint other)
    {
        connectedTo = other;

        if (partPointGUI !=null)
            partPointGUI.gameObject.SetActive(other == null);

        foreach (GameObject go in activateOnDisconnect)
            go.SetActive(other == null);

        part.OnPartPointConnect.Invoke();
    }

    public void Disconnect()
    {
        if (connectedTo != null )
        {
            connectedTo.ConnectTo(null);
            ConnectTo(null);
        }
    }

    public bool Connected
    {
        get
        {
            return connectedTo != null;
        }
    }

    public bool ConnectedToConnection(string connection)
    {
        if (connectedTo != null)
        {
            var points = connectedTo.part.partPoints.Where(x => x.connection == connection);
            return points.Count()>0;
        }
        return false;
    }

    public bool ConnectedToPartCategory(string category)
    {
        if (connectedTo!=null)
        {
            if (connectedTo.part.partCategory==category)
            {
                return true;
            }
        }
        return false;
    }

    private void OnDestroy()
    {
        if (partPointGUI == null)
            return;

        if (partPointGUI.gameObject == null)
            return;

        if (partPointGUI.gameObject.activeInHierarchy)
            Destroy( partPointGUI.gameObject );
    }

    private void OnDisable()
    {
        EnableConnectedTo(false);
    }

    private void OnEnable()
    {
        EnableConnectedTo(true);
    }

    void EnableConnectedTo(bool enable)
    {
        if (part.type != 2 && connectedTo != null)
        {
            if (connectedTo.gameObject.activeInHierarchy!=enable)
            {
                connectedTo.part.gameObject.SetActive(enable);
            }
        }
    }
}
