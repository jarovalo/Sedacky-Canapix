﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotator : MonoBehaviour {

    public Transform rotateObject;

	void LateUpdate () {
        if (rotateObject == null)
            return;

        if (Input.touchCount != 2)
            return;

        Touch touchOne=Input.GetTouch(0);
        Touch touchTwo= Input.GetTouch(1);

        Touch touchRight = touchOne.position.x > touchTwo.position.x ? touchOne : touchTwo;
        Touch touchLeft = touchOne.position.x < touchTwo.position.x ? touchOne : touchTwo;

        Touch touchTop = touchOne.position.y > touchTwo.position.y ? touchOne : touchTwo;
        Touch touchBottom = touchOne.position.y < touchTwo.position.y ? touchOne : touchTwo;

        float direction = -touchRight.deltaPosition.y;
        direction += touchLeft.deltaPosition.y;
        direction += touchTop.deltaPosition.x;
        direction -= touchBottom.deltaPosition.x;

        rotateObject.Rotate(new Vector3(0, direction*Time.deltaTime*5f, 0));
    }
}
