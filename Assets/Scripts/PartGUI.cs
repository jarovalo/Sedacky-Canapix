﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PartGUI : MonoBehaviour
{
    public Image image;
    public TMPro.TextMeshProUGUI text;
    public GameObject imageLocked;

    public Part part;

    public Button OptionalFunctionNone;

    public void Init(Part part)
    {
        this.part = part;

        image.sprite = part.image;
        text.text = part.partName;
        imageLocked.SetActive(false);

        ShowOptionalFunctions(false);

        foreach (PartOptionalFunctionScriptableObject optionalFunction in part.possibleOptionalFunctions)
        {
            Button newButton = Instantiate(OptionalFunctionNone, OptionalFunctionNone.transform.parent);
            LocalizedText text = newButton.GetComponentInChildren<LocalizedText>();
            text.SetLocalizedTexts(optionalFunction.localizations);
            newButton.onClick.AddListener(() => {
                Configurator.instance.AddPart(part, optionalFunction);
                ShowOptionalFunctions(false);
            });
        }

        if (OptionalFunctionNone!=null)
        {
            OptionalFunctionNone.onClick.AddListener(() => {
                Configurator.instance.AddPart(part, null);
                ShowOptionalFunctions(false);
            });
        }
    }

    public void SetInteractable(bool interactable)
    {
        GetComponent<Button>().interactable = interactable;
        imageLocked.SetActive(interactable == false);
    }

    public void OnClick()
    {
        Configurator.instance.DisableOptionalFunctionsGUI(this);

        bool optionalFunctionsActive = false;
        if (OptionalFunctionNone!=null)
        {
            optionalFunctionsActive = OptionalFunctionNone.transform.parent.gameObject.activeInHierarchy;
        }

        if (optionalFunctionsActive)
        {
            ShowOptionalFunctions(false);
        }
        else
        {
            if (part.possibleOptionalFunctions.Length == 0)
            {
                Configurator.instance.AddPart(part, null);
            }
            else
            {
                ShowOptionalFunctions(true);
            }
        }
    }

    public void ShowOptionalFunctions(bool show)
    {
        if (OptionalFunctionNone!=null)
            OptionalFunctionNone.transform.parent.gameObject.SetActive(show);
    }
}
