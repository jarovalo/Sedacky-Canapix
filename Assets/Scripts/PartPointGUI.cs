﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PartPointGUI : MonoBehaviour
{
    public PartPoint partPoint;

    public Image image;

    public void Init(PartPoint partPoint, Sprite sprite)
    {
        this.partPoint = partPoint;
        image.sprite = sprite;
        partPoint.partPointGUI = this;

        FollowTransform followTransform = gameObject.AddComponent<FollowTransform>();
        followTransform.follow = partPoint.transform;
    }
}
