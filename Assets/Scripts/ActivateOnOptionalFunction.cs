﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ActivateOnOptionalFunction : MonoBehaviour
{
    public PartOptionalFunctionScriptableObject[] optionalFunctions;

    // Start is called before the first frame update
    void Start()
    {
        Part part = GetComponentInParent<Part>();
        bool activate = optionalFunctions.Contains(part.selectedOptionalFunction);
        gameObject.SetActive(activate);
    }
}
