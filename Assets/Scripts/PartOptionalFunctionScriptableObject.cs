﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PartOptionalFunction", menuName = "ScriptableObjects/PartOptionalFunctionScriptableObject", order = 5)]
public class PartOptionalFunctionScriptableObject : ScriptableObject
{
    public string id;
    public LocalizationManager.Localization[] localizations;
}
