﻿using UnityEngine;

[CreateAssetMenu(fileName = "Material", menuName = "ScriptableObjects/MaterialScriptableObject", order = 3)]
public class MaterialScriptableObject : ScriptableObject
{
    public Material material;
    public Material materialShelf;
    public Sprite image;
    public string materialName;
    public ColorSetScriptableObject colors;
}