﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Screenshot : MonoBehaviour
{
    public Camera[] cameras;

    public event Action<Texture2D[]> ScreenshotsTaken;

    public int resWidth = 1280;
    public int resHeight = 1280;

    public int finalSize = 512;

    private bool takingScreenshots = false;

    public void TakeScreenshots()
    {
        takingScreenshots = true;
    }

    void LateUpdate()
    {
        if (takingScreenshots)
        {
            Texture2D[] screenshots = new Texture2D[cameras.Length];

            for(int i=0;i<cameras.Length;i++)
            {
                Camera camera = cameras[i];
                RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
                camera.targetTexture = rt;
                Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGBA32, false);
                camera.Render();
                RenderTexture.active = rt;
                screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
                camera.targetTexture = null;
                RenderTexture.active = null; // JC: added to avoid errors
                Destroy(rt);

                screenshots[i] = screenShot;
            }
            takingScreenshots = false;

            RemoveBorders(screenshots, finalSize);

            ScreenshotsTaken.Invoke(screenshots);
        }
    }

    public static void RemoveBorders(Texture2D[] screenshots, int finalSize)
    {
        for (int i= 0;i < screenshots.Length;i++)
        {
            screenshots[i]=RemoveBorders(screenshots[i], finalSize);
        }
    }

    public static Texture2D RemoveBorders(Texture2D texture, int finalSize)
    {
        int minX = int.MaxValue;
        int minY = int.MaxValue;
        int maxX = int.MinValue;
        int maxY = int.MinValue;
        for (int x = 0; x < texture.width; x++)
        {
            for (int y = 0; y < texture.height; y++)
            {
                if (texture.GetPixel(x, y).a > 0f)
                {
                    if (x < minX)
                        minX = x;
                    if (x > maxX)
                        maxX = x;
                    if (y < minY)
                        minY = y;
                    if (y > maxY)
                        maxY = y;
                }
            }
        }
        int width = maxX - minX + 1;
        int height = maxY - minY + 1;
        Texture2D newScreenshot = new Texture2D(width, height);
        newScreenshot.SetPixels(texture.GetPixels(minX, minY, width, height));

        if (width > finalSize || height > finalSize)
        {
            int w = finalSize;
            int h = Mathf.RoundToInt(finalSize * ((float)height / (float)width));
            if (width < height)
            {
                w = Mathf.RoundToInt(finalSize * ((float)width / (float)height));
                h = finalSize;
            }
            TextureScale.Bilinear(newScreenshot, w, h);
        }

        return newScreenshot;
    }

    
}
