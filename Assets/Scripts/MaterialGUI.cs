﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MaterialGUI : MonoBehaviour
{
    public Image image;
    public TMPro.TextMeshProUGUI text;

    public MaterialScriptableObject material;

    public ColorSet colorSet;

    public bool printMaterialName=true;

    public int index;

    public void Init(MaterialScriptableObject material,ColorSet colorSet, string name ,int index=-1)
    {
        this.index = index;
        this.material = material;
        this.colorSet = colorSet;

        if (material!=null)
        {
            image.sprite = material.image;
            if (colorSet != null)
            {
                if (colorSet.sprite != null)
                {
                    image.sprite = colorSet.sprite;
                }

                image.color = colorSet.color;
            }
        }
        /*
        string colorName = "";
        if (colorSet!=null)
        {
            colorName = colorSet.colorName;
        }
        string materialName = colorName; 

        if (printMaterialName)
            materialName = material.materialName+ " " + colorName;

        if (name.Length > 0)
            materialName = name;

        text.text = materialName;
            */

        text.text = name;
    }

    public void Init(Sprite sprite, string name, int index = -1)
    {
        this.index = index;

        image.sprite = sprite;

        text.text = name;
    }



}
