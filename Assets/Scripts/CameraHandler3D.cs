﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraHandler3D : MonoBehaviour
{
    private bl_CameraOrbit cameraOrbit;

    private void Awake()
    {
        cameraOrbit = GetComponent<bl_CameraOrbit>();
        cameraOrbit.isForMobile = Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            cameraOrbit.enabled = Utils.IsPointerOverUIObject() == false;
            cameraOrbit.StartFov = Camera.main.fieldOfView;
        }
        if (Input.GetMouseButtonUp(0))
        {
            cameraOrbit.enabled = true;
        }
    }
}
