﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selector : MonoBehaviour
{
    public LayerMask layerPart;
    public Configurator configurator;

    float touchDownTime;
    Vector3 touchDownPos;
    Part touchDownPart;

    // Update is called once per frame
    void Update()
    {
        if (Utils.IsPointerOverUIObject())
            return;

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit,layerPart))
            {
                touchDownTime = Time.time;
                touchDownPos = Input.mousePosition;
                touchDownPart = hit.transform.GetComponentInParent<Part>();
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (Time.time-touchDownTime<0.3f)
            {
                if (touchDownPart != null)
                {
                    Vector2 diff = Input.mousePosition - touchDownPos;
                    if (diff.magnitude < Screen.width / 100f)
                    {
                        configurator.SelectPart(touchDownPart);
                    }
                }
                else
                {
                    configurator.SelectPart(null);
                    configurator.ShowLegListMaterials(false);
                }
            }
        }
    }
}
