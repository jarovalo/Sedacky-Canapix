﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshCollider))]
public class ActivateOnClick : MonoBehaviour
{
    public GameObject ActivateObject;
    public bool activate = true;

    private void OnMouseUp()
    {
        if (CameraController.IsCamera2D)
            return;

        if (ActivateObject.activeSelf!=activate)
            ActivateObject.SetActive(activate);
        else
            ActivateObject.SetActive(!activate);
    }
}
