﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PartAnimation : MonoBehaviour
{
    enum AnimationState
    {
        Closed,
        Opened
    }

    public Vector3 rotationOpen = Vector3.zero;
    public Vector3 rotationClosed=Vector3.zero;

    public Vector3 positionOpen = Vector3.zero;
    public Vector3 positionClosed = Vector3.zero;

    public float speed = 2f;

    public Transform connectedObject;

    AnimationState state = AnimationState.Closed;

    bool animating = false;
    float animatingStartsAt = 0f;

    private void Start()
    {
        state = transform.localEulerAngles == rotationOpen ? AnimationState.Opened : AnimationState.Closed;
    }

    private void Update()
    {
        Animate();
    }

    private void OnMouseUp()
    {
        if (CameraController.IsCamera2D)
            return;

        if (this.enabled)
            Toggle();
    }

    public void Toggle()
    {
        if (state == AnimationState.Closed)
            state = AnimationState.Opened;
        else
            state = AnimationState.Closed;

        animating = true;
        animatingStartsAt = Time.time;
    }

    void Animate()
    {
        if (animating)
        {
            // rotation
            if (rotationOpen != rotationClosed)
            {
                Vector3 rotFrom;
                Vector3 rotTo;
                if (state == AnimationState.Opened)
                {
                    rotFrom = rotationClosed;
                    rotTo = rotationOpen;
                }
                else
                {
                    rotFrom = rotationOpen;
                    rotTo = rotationClosed;
                }

                Vector3 rot = Vector3.Lerp(rotFrom, rotTo, (Time.time - animatingStartsAt) / speed);
                if (rot == rotTo)
                {
                    animating = false;
                    if (connectedObject != null)
                    {
                        connectedObject.rotation = transform.rotation;
                    }
                }

                transform.localEulerAngles = rot;
            }

            // translation
            if (positionOpen != positionClosed)
            {
                Vector3 posFrom;
                Vector3 posTo;
                if (state == AnimationState.Opened)
                {
                    posFrom = positionClosed;
                    posTo = positionOpen;
                }
                else
                {
                    posFrom = positionOpen;
                    posTo = positionClosed;
                }

                Vector3 pos = Vector3.Lerp(posFrom, posTo, (Time.time - animatingStartsAt) / speed);
                if (pos == posTo)
                {
                    animating = false;
                    if (connectedObject != null)
                    {
                        connectedObject.localPosition = transform.localPosition;
                    }
                }

                transform.localPosition = pos;
            }
        }
    }

    private void Reset()
    {
        rotationOpen = transform.localEulerAngles;
        rotationClosed = rotationOpen;

        positionOpen = transform.localPosition;
        positionClosed = positionOpen;
    }

}
