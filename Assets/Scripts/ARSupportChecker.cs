﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.XR.ARFoundation;

public class ARSupportChecker : MonoBehaviour
{
    public AppConfigGlobalScriptableObject appConfig;
    IEnumerator Start()
    {
        if (ARSession.state == ARSessionState.None ||
            ARSession.state == ARSessionState.CheckingAvailability)
        {
            yield return ARSession.CheckAvailability();
        }

        if (ARSession.state == ARSessionState.Unsupported)
        {
            NPBinding.UI.ShowAlertDialogWithSingleButton("Rozšírená realita", "Toto zariadenie nepodporuje rozšírenú realitu", "Ok", OnButtonPressed);
        }
    }

    private void OnButtonPressed(string _buttonPressed)
    {
        SceneManager.LoadScene(appConfig.appConfig.configuratorScene);
    }
}
