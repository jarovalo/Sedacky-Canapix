﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class AssignParts : EditorWindow
{
    Object model;
    Object[] folder=new Object[2];

    // Add menu named "My Window" to the Window menu
    [MenuItem("Yahart/Assign Parts")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        AssignParts window = (AssignParts)EditorWindow.GetWindow(typeof(AssignParts));
        window.Show();
    }

    void OnGUI()
    {
        model=EditorGUILayout.ObjectField("Model scriptable object",model,typeof(Object));

        for(int i=0;i<folder.Length;i++)
        {
            folder[i]= EditorGUILayout.ObjectField("Folder "+i.ToString(),folder[i], typeof(Object));
        }

        if (GUILayout.Button("Assign"))
        {
            ModelScriptableObject mso = (ModelScriptableObject)model;
            List<string> parts = new List<string>();

            for (int i=0;i<folder.Length;i++)
            {
                if (folder[i] != null)
                {
                    //string path = System.IO.Path.GetDirectoryName(AssetDatabase.GetAssetPath(mso)) + "/" + mso.modelName + "/Parts";
                    string path = AssetDatabase.GetAssetPath(folder[i]);
                    string[] files = System.IO.Directory.GetFiles(path, "*.prefab");

                    foreach (string file in files)
                    {
                        parts.Add(System.IO.Path.GetFileNameWithoutExtension(file));
                    }
                }
            }

            mso.parts = parts.ToArray();
            EditorUtility.SetDirty(mso);
        }
    }
}