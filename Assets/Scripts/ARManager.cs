﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Experimental.XR;
using UnityEngine.SceneManagement;
#if !UNITY_WEBGL
using UnityEngine.XR.ARFoundation;
#endif 

public class ARManager : MonoBehaviour
{
    GameObject configuratorRoot;

#if !UNITY_WEBGL && !UNITY_STANDALONE_WIN
    public AppConfigGlobalScriptableObject appConfig;


    public ARPlaneManager arPlaneManager;
    public ARRaycastManager arRaycastManager;
    public ObjectMover objectMover;
    public ObjectRotator objectRotator;

    public LayerMask layerMaskPart;

    public GameObject findSurface;

    public GameObject floor;

    public Button buttonPlace;
    public Button buttonCamera;

    int state = 0;
    TrackableCollection<ARPlane> planes = new TrackableCollection<ARPlane>();

    public Animator configuredObjectParent;

    GameObject configuredObject;

    static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();

    private void Start()
    {
        Input.simulateMouseWithTouches = true;

        configuredObject = GameObject.FindGameObjectWithTag("ConfiguredObject");
        SceneManager.MoveGameObjectToScene(configuredObject, SceneManager.GetSceneByName("ar"));
        configuredObject.transform.SetParent(configuredObjectParent.transform);
        configuredObject.transform.localPosition = Vector3.zero;
        configuredObject.transform.localRotation = Quaternion.identity;
        objectMover.moveObject = configuredObject.transform;
        objectMover.enabled = false;

        objectRotator.rotateObject = configuredObject.transform;
        objectRotator.enabled = false;

        floor.SetActive(false);

        buttonPlace.gameObject.SetActive(false);
        buttonCamera.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (state == 0)
        {
            DetectPlanes();
        }

        if (state == 1)
        {
            CheckManipulation();
        }

        if (Input.GetKeyUp(KeyCode.Escape))
            BackToConfig();
    }

    void CheckManipulation()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray,100f,layerMaskPart))
            {
                objectMover.enabled = true;
                objectRotator.enabled = true;
                configuredObjectParent.SetInteger("state",1);
                buttonPlace.gameObject.SetActive(true);
                buttonCamera.gameObject.SetActive(false);
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            objectMover.enabled = false;
            objectRotator.enabled = false;
        }
    }

    public void ButtonPlace()
    {
        configuredObjectParent.SetInteger("state", 0);
        buttonPlace.gameObject.SetActive(false);
        buttonCamera.gameObject.SetActive(true);
    }

    void DetectPlanes()
    {
        if (arRaycastManager.Raycast( new Vector3(Screen.width/2,Screen.height/2,0), s_Hits,UnityEngine.XR.ARSubsystems.TrackableType.PlaneWithinPolygon))
        {
            // Raycast hits are sorted by distance, so the first one
            // will be the closest hit.
            var hitPose = s_Hits[0].pose;
            PlaceObject(hitPose.position, hitPose.rotation);
            state = 1;
        }
    }

    void PlaceObject(Vector3 pos,Quaternion rot)
    {
        floor.transform.position = pos;
        floor.transform.rotation = rot;
        floor.SetActive(true);

        RemovePlanes();
        findSurface.SetActive(false);

        Handheld.Vibrate();

        buttonCamera.gameObject.SetActive(true);
    }

    void RemovePlanes()
    {
        planes= arPlaneManager.trackables;

        foreach (ARPlane plane in planes)
        {
            Destroy(plane.gameObject);
        }

        arPlaneManager.enabled = false;
    }

    float PlaneSize(ARPlane plane)
    {
        return plane.size.y * plane.size.x;
    }

    public void BackToConfig()
    {
        Scene sceneConfigurator = SceneManager.GetSceneByName(appConfig.appConfig.configuratorScene);
        configuredObject.transform.SetParent(null);
        SceneManager.MoveGameObjectToScene(configuredObject.gameObject, sceneConfigurator);
        configuredObject.transform.position = Vector3.zero;
        configuredObject.transform.rotation = Quaternion.identity;

        configuratorRoot.SetActive(true);

        Scene sceneAR = SceneManager.GetSceneByName("ar");
        SceneManager.UnloadSceneAsync(sceneAR);
    }
#endif

    public void DisableConfigurator()
    {
        configuratorRoot = GameObject.FindGameObjectWithTag("ConfiguratorRoot");
        configuratorRoot.SetActive(false);
    }

}
